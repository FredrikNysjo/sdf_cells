cmake_minimum_required(VERSION 3.10.0)

# Specify project name
project(sdf_cells)

# Specify build type
set(CMAKE_BUILD_TYPE RelWithDebInfo)

# Set extra compiler flags
if(MSVC)
  set(CMAKE_CXX_FLAGS "/W3 /EHsc /std:c++14 /openmp")
  add_definitions("-D_CRT_SECURE_NO_WARNINGS")
else(MSVC)
  set(CMAKE_CXX_FLAGS "-W -Wall -Wno-missing-braces -std=c++14 -fopenmp")
endif(MSVC)

# Add source directories
aux_source_directory("${CMAKE_CURRENT_SOURCE_DIR}/src" PROJECT_SRCS)

# Add include directories
include_directories("${CMAKE_CURRENT_SOURCE_DIR}/src")

# Third-party dependencies
add_subdirectory("${CMAKE_CURRENT_SOURCE_DIR}/third_party" ${CMAKE_CURRENT_BINARY_DIR}/third_party)
include_directories(SYSTEM ${THIRD_PARTY_SYSTEM_INCLUDE_DIRS})
add_definitions(${THIRD_PARTY_DEFINITIONS})

# ISPC kernels
include(./src/ispc/CMakeLists.txt)

# Create build files for executable
add_executable(${PROJECT_NAME} ${PROJECT_SRCS} ${ISPC_KERNELS})

# Link against libraries
target_link_libraries(${PROJECT_NAME} third_party glfw ${THIRD_PARTY_LIBRARIES} ${GLFW_LIBRARIES})

# Install executable
install(TARGETS ${PROJECT_NAME} DESTINATION bin)
