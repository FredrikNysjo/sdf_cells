/// @file    main.cpp
/// @author  Fredrik Nysjö
///

#include "gl_utils.h"
#include "gl_state.h"
#include "vtk_utils.h"

#include <GL/gl3w.h>
#include <GLFW/glfw3.h>

#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/random.hpp>
#include <glm/gtx/component_wise.hpp>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#include <cstdlib>
#include <cstdio>
#include <algorithm>
#include <random>
#include <unordered_map>

#undef NDEBUG
#include <cassert>

#define CLUSTER_SIZE 64
#define MAX_NUM_INSTANCES (1 << 15)
#define MAX_INSTANCE_BUFFER_SIZE (MAX_NUM_INSTANCES * 64)

#define SHAPE_SQUARE 0
#define SHAPE_ROUND 1
#define SHAPE_DISK 2
#define SHAPE_BOX 3
#define SHAPE_CELL 4

struct SDFCells {
    std::vector<glm::uvec4> cells;
    std::vector<glm::uvec4> bricks;
    std::vector<glm::uvec4> lodinfo;
};

struct DrawableInstances {
    glm::ivec3 grid_dim = glm::ivec3(8, 8, 8);
    std::vector<glm::mat4> world_from_local;
    std::vector<glm::uvec4> indirect_cmds;
};

struct SceneUniforms {
    glm::mat4 view_from_world;
    glm::mat4 proj_from_view;
    glm::vec2 resolution;
    float time = 0.0f;
    int frame = 0;
    HemisphereLight hemisphere;
    Light light;
};

struct Camera {
    glm::mat4 proj_from_view;
    glm::mat4 view_from_world;
    glm::vec3 location = glm::vec3(0.0f, 0.0f, 1.6f);
};

struct Interactor {
    cg::CgTrackball trackball;
    bool panning = false;
    glm::vec2 pan_center = glm::vec2(0.0f, 0.0f);
};

struct Window {
    int width = 1000;
    int height = 700;
    GLFWwindow *id = nullptr;
    float elapsed_time = 0.0f;
    uint32_t idle_frames = 0;
    bool update_when_idle = true;
    int swap_interval = 1;
};

struct Settings {
    glm::vec3 bg_color0 = glm::vec3(0.0f);
    glm::vec3 bg_color1 = glm::vec3(1.0f);
    glm::vec3 base_color = glm::vec3(0.9f, 0.7f, 0.1f);
    float roughness = 0.6f;
    bool show_normals = false;
    bool show_visibility_id = false;
    bool show_cell_id = false;
    bool show_lod = true;
    bool fill_holes = true;
    bool blend_normals = false;
    bool use_indirect_draw = false;
    bool use_atomic_splatting = true;
    bool freeze_visibility = false;
    bool fxaa_enabled = true;
    bool gamma_enabled = true;
    int32_t point_shape = SHAPE_ROUND;
    float point_scale = 1.4f;
    int32_t lod_threshold_pixels = 1;
    std::string input_filename = "../data/bunny_128_sdf.vtk";
};

struct Context {
    Window window;
    Settings settings;

    std::unordered_map<std::string, GLuint> programs;
    std::unordered_map<std::string, GLuint> vaos;
    std::unordered_map<std::string, GLuint> buffers;
    std::unordered_map<std::string, GLuint> textures;
    std::unordered_map<std::string, GLuint> framebuffers;
    std::unordered_map<std::string, GLPipelineStateInfo> pipelines;
    std::unordered_map<std::string, GLTimerQuery> timers;

    Camera camera;
    Interactor interactor;
    SceneUniforms scene_uniforms;
    DrawableInstances drawable_instances;
    cg::VolumeUInt8 volume;
    std::vector<cg::VolumeUInt8> mipmap;
    SDFCells cells;
};

std::string shaderDir(void)
{
    std::string root_dir = cg::cgGetEnvSafe("SDF_CELLS_ROOT");
    if (root_dir.empty()) { root_dir = ".."; }
    return root_dir + "/src/shaders/";
}

void createFramebuffer(GLuint *framebuffer, int width, int height, GLuint *color_texture,
                       GLuint *depth_texture, GLuint *gbuffer_texture, GLuint *id_texture,
                       GLuint *depth2_texture)
{
    glDeleteTextures(1, color_texture);
    glCreateTextures(GL_TEXTURE_2D, 1, color_texture);
    glTextureStorage2D(*color_texture, 1, GL_RGBA16F, width, height);

    glDeleteTextures(1, depth_texture);
    glCreateTextures(GL_TEXTURE_2D, 1, depth_texture);
    glTextureStorage2D(*depth_texture, 1, GL_DEPTH24_STENCIL8, width, height);

    glDeleteTextures(1, gbuffer_texture);
    glCreateTextures(GL_TEXTURE_2D, 1, gbuffer_texture);
    glTextureStorage2D(*gbuffer_texture, 1, GL_RGBA16F, width, height);

    glDeleteTextures(1, id_texture);
    glCreateTextures(GL_TEXTURE_2D, 1, id_texture);
    glTextureStorage2D(*id_texture, 1, GL_RG32F, width, height);

    glDeleteTextures(1, depth2_texture);
    glCreateTextures(GL_TEXTURE_2D, 1, depth2_texture);
    glTextureStorage2D(*depth2_texture, 1, GL_R32F, width, height);

    glDeleteFramebuffers(1, framebuffer);
    glCreateFramebuffers(1, framebuffer);
    glNamedFramebufferTexture(*framebuffer, GL_COLOR_ATTACHMENT0, *color_texture, 0);
    glNamedFramebufferTexture(*framebuffer, GL_COLOR_ATTACHMENT3, *gbuffer_texture, 0);
    glNamedFramebufferTexture(*framebuffer, GL_COLOR_ATTACHMENT4, *id_texture, 0);
    glNamedFramebufferTexture(*framebuffer, GL_COLOR_ATTACHMENT5, *depth2_texture, 0);
    glNamedFramebufferTexture(*framebuffer, GL_DEPTH_STENCIL_ATTACHMENT, *depth_texture, 0);
    GLenum status = glCheckNamedFramebufferStatus(*framebuffer, GL_DRAW_FRAMEBUFFER);
    if (status != GL_FRAMEBUFFER_COMPLETE) {
        std::fprintf(stderr, "Error: Incomplete framebuffer\n");
    }
}

void createUniformBuffer(GLuint *buffer, int num_bytes = 65536)
{
    glDeleteBuffers(1, buffer);
    glCreateBuffers(1, buffer);
    glBindBuffer(GL_COPY_WRITE_BUFFER, *buffer);  // Workaround for DSA issue
    glNamedBufferData(*buffer, num_bytes, nullptr, GL_DYNAMIC_DRAW);
}

void createSDFCellBuffer(GLuint *cell_buffer, GLuint *cell_texture,
                         const std::vector<glm::uvec4> &cells)
{
    glDeleteBuffers(1, cell_buffer);
    glCreateBuffers(1, cell_buffer);
    glBindBuffer(GL_COPY_WRITE_BUFFER, *cell_buffer);  // Workaround for DSA issue
    const size_t num_bytes = cells.size() * sizeof(cells[0]);
    glNamedBufferData(*cell_buffer, num_bytes, &cells[0], GL_STATIC_DRAW);

    glDeleteTextures(1, cell_texture);
    glCreateTextures(GL_TEXTURE_BUFFER, 1, cell_texture);
    glTextureBuffer(*cell_texture, GL_RGBA32UI, *cell_buffer);
}

void createIndirectBuffer(GLuint *buffer, GLuint *texture)
{
    glDeleteBuffers(1, buffer);
    glCreateBuffers(1, buffer);
    glBindBuffer(GL_COPY_WRITE_BUFFER, *buffer);  // Workaround for DSA issue
    size_t num_bytes = MAX_NUM_INSTANCES * sizeof(glm::uvec4);
    glNamedBufferData(*buffer, num_bytes, nullptr, GL_STATIC_DRAW);
    glClearNamedBufferData(*buffer, GL_RGBA32UI, GL_RGBA_INTEGER, GL_UNSIGNED_INT, nullptr);

    glDeleteTextures(1, texture);
    glCreateTextures(GL_TEXTURE_BUFFER, 1, texture);
    glTextureBuffer(*texture, GL_RGBA32UI, *buffer);
}

void createVisibilityBuffer(GLuint *buffer, GLuint *texture)
{
    glDeleteBuffers(1, buffer);
    glCreateBuffers(1, buffer);
    glBindBuffer(GL_COPY_WRITE_BUFFER, *buffer);  // Workaround for DSA issue
    size_t num_bytes = MAX_NUM_INSTANCES * 512;   // 512 bytes needed for visibility per instance
    glNamedBufferData(*buffer, num_bytes, nullptr, GL_STATIC_DRAW);
    glClearNamedBufferData(*buffer, GL_R32UI, GL_RED_INTEGER, GL_UNSIGNED_INT, nullptr);

    glDeleteTextures(1, texture);
    glCreateTextures(GL_TEXTURE_BUFFER, 1, texture);
    glTextureBuffer(*texture, GL_R32UI, *buffer);
}

void setupGLPrograms(Context &ctx)
{
    // Delete programs
    for (auto &pi : ctx.programs) { glDeleteProgram(pi.second); }

    ctx.programs["fxaa_gamma"] = cg::cgLoadShaderProgram(shaderDir() + "fxaa_gamma.glsl");
    ctx.programs["draw_cells_visible_ts"] = cg::cgLoadShaderProgram(
        shaderDir() + "draw_cells_unified_ts.glsl", "#version 450\n#define DRAW_CELLS_VISIBLE");
    ctx.programs["draw_cells_hidden_ts"] = cg::cgLoadShaderProgram(
        shaderDir() + "draw_cells_unified_ts.glsl", "#version 450\n#define DRAW_CELLS_HIDDEN");
    ctx.programs["draw_cells_ewa_ts"] = cg::cgLoadShaderProgram(
        shaderDir() + "draw_cells_unified_ts.glsl", "#version 450\n#define DRAW_CELLS_EWA");
    ctx.programs["draw_cells_atomic_fs"] =
        cg::cgLoadShaderProgram(shaderDir() + "draw_cells_atomic_fs.glsl");
    ctx.programs["draw_cells_atomic_ts"] =
        cg::cgLoadShaderProgram(shaderDir() + "draw_cells_atomic_ts.glsl");
    ctx.programs["draw_cells_atomic_ts2"] =
        cg::cgLoadShaderProgram(shaderDir() + "draw_cells_atomic_ts2.glsl");
    ctx.programs["update_indirect_cs"] =
        cg::cgLoadShaderProgram(shaderDir() + "update_indirect_cs.glsl");
    ctx.programs["visibility_cs"] = cg::cgLoadShaderProgram(shaderDir() + "visibility_cs.glsl");
    ctx.programs["deferred_cs"] = cg::cgLoadShaderProgram(shaderDir() + "deferred_cs.glsl");
}

void setupGLPipelines(Context &ctx)
{
    glClearCurrentPipelineState();
    glUseProgram(ctx.programs["fxaa_gamma"]);
    glDepthMask(GL_FALSE);
    glCaptureCurrentPipelineState(&ctx.pipelines["fxaa_gamma"]);

    glClearCurrentPipelineState();
    glUseProgram(ctx.programs["draw_cells_visible_ts"]);
    glEnable(GL_PROGRAM_POINT_SIZE);
    glEnable(GL_DEPTH_TEST);
    glCaptureCurrentPipelineState(&ctx.pipelines["draw_cells_visible_ts"]);

    glClearCurrentPipelineState();
    glUseProgram(ctx.programs["draw_cells_hidden_ts"]);
    glEnable(GL_PROGRAM_POINT_SIZE);
    glEnable(GL_DEPTH_TEST);
    glDepthMask(GL_FALSE);
    glCaptureCurrentPipelineState(&ctx.pipelines["draw_cells_hidden_ts"]);

    glClearCurrentPipelineState();
    glUseProgram(ctx.programs["draw_cells_ewa_ts"]);
    glEnable(GL_PROGRAM_POINT_SIZE);
    glEnable(GL_DEPTH_TEST);
    glDepthMask(GL_FALSE);
    glEnable(GL_BLEND);
    glBlendFuncSeparate(GL_ONE, GL_ONE, GL_ONE, GL_ONE);
    glCaptureCurrentPipelineState(&ctx.pipelines["draw_cells_ewa_ts"]);

    glClearCurrentPipelineState();
    glUseProgram(ctx.programs["draw_cells_atomic_fs"]);
    glEnable(GL_PROGRAM_POINT_SIZE);
    glDisable(GL_DEPTH_TEST);
    glDepthMask(GL_FALSE);
    glCaptureCurrentPipelineState(&ctx.pipelines["draw_cells_atomic_fs"]);

    glClearCurrentPipelineState();
    glUseProgram(ctx.programs["draw_cells_atomic_ts"]);
    glEnable(GL_PROGRAM_POINT_SIZE);
    glDisable(GL_DEPTH_TEST);
    glDepthMask(GL_FALSE);
    glCaptureCurrentPipelineState(&ctx.pipelines["draw_cells_atomic_ts"]);

    glClearCurrentPipelineState();
    glUseProgram(ctx.programs["draw_cells_atomic_ts2"]);
    glEnable(GL_PROGRAM_POINT_SIZE);
    glDisable(GL_DEPTH_TEST);
    glDepthMask(GL_FALSE);
    glCaptureCurrentPipelineState(&ctx.pipelines["draw_cells_atomic_ts2"]);

    glClearCurrentPipelineState();
}

void setupGLFramebuffers(Context &ctx)
{
    createFramebuffer(&ctx.framebuffers["forward"], ctx.window.width, ctx.window.height,
                      &ctx.textures["color"], &ctx.textures["depth"], &ctx.textures["gbuffer"],
                      &ctx.textures["id"], &ctx.textures["depth2"]);
}

void setupGLBuffers(Context &ctx)
{
    createUniformBuffer(&ctx.buffers["scene_uniforms"]);
    createUniformBuffer(&ctx.buffers["drawable_instances"], MAX_INSTANCE_BUFFER_SIZE);
}

void setupGLQueries(Context &ctx)
{
    // Delete queries
    for (auto &ti : ctx.timers) { glDeleteQueries(1, &ti.second.query); }

    glCreateQueries(GL_TIME_ELAPSED, 1, &ctx.timers["rasterise"].query);
    glCreateQueries(GL_TIME_ELAPSED, 1, &ctx.timers["visibility"].query);
    glCreateQueries(GL_TIME_ELAPSED, 1, &ctx.timers["deferred"].query);
    glCreateQueries(GL_TIME_ELAPSED, 1, &ctx.timers["fxaa_gamma"].query);
}

void setupTrackball(Context &ctx)
{
    ctx.interactor.trackball.radius = double(std::min(ctx.window.width, ctx.window.height)) / 2.0;
    ctx.interactor.trackball.center = glm::vec2(ctx.window.width, ctx.window.height) / 2.0f;
}

void setupCameras(Context &ctx)
{
    float aspect = float(ctx.window.width) / ctx.window.height;
    ctx.camera.proj_from_view = glm::perspective(glm::radians(45.0f), aspect, 0.1f, 100.0f);
}

glm::ivec3 mortonToXYZ(int32_t index)
{
    glm::ivec3 p = glm::ivec3(0);
    for (int32_t i = 0; i < 10; ++i) {
        p.x |= ((index >> (3 * i + 0)) & 1) << i;
        p.y |= ((index >> (3 * i + 1)) & 1) << i;
        p.z |= ((index >> (3 * i + 2)) & 1) << i;
    }
    return p;
}

uint32_t computeBrick(const std::vector<cg::VolumeUInt8> &mipmap, int32_t level, int32_t xmin,
                      int32_t xmax, int32_t ymin, int32_t ymax, int32_t zmin, int32_t zmax,
                      SDFCells &cells)
{
    const glm::ivec3 image_dim = mipmap[level].base.dimensions;
    const glm::ivec3 brick_dim = glm::ivec3(xmax - xmin, ymax - ymin, zmax - zmin);
    assert(brick_dim.x == brick_dim.y && brick_dim.x == brick_dim.z);

    const int32_t num_voxels = brick_dim.x * brick_dim.y * brick_dim.z;
    const int32_t isovalue = 128;
    const float pot = glm::exp2(float(level));
    const float scale = 1.0f / pot;

    std::vector<glm::uvec4> buffer;
    buffer.reserve(4096);
    for (int32_t index = 0; index < num_voxels; ++index) {
        const glm::ivec3 q = mortonToXYZ(index);
        const glm::ivec3 p = q + glm::ivec3(xmin, ymin, zmin);
        if (glm::any(glm::greaterThan(p, image_dim - 1))) continue;

        const int32_t value = mipmap[level](p.x, p.y, p.z);
        if (glm::abs(value - isovalue) > 12) continue;

        float c[8] = {0.0f};  // Interpolated cell corners
        for (int32_t k = 0; k < 2; ++k)
            for (int32_t j = 0; j < 2; ++j)
                for (int32_t i = 0; i < 2; ++i) {
                    float value = cg::volumeInterpolate(mipmap[0], (p.x + i) * pot, (p.y + j) * pot,
                                                        (p.z + k) * pot);
                    c[k * 4 + j * 2 + i] =
                        glm::max(0.0f, glm::min(255.0f, (value - 128.0f) * scale + 128.0f));
                }

        float range[2] = {c[0], c[0]};
        for (int32_t i = 1; i < 8; ++i) {
            range[0] = std::min(range[0], c[i]);
            range[1] = std::max(range[1], c[i]);
        }
        if (range[0] > isovalue || range[1] <= isovalue) continue;

        glm::uvec4 cell = glm::uvec4(0u);
        cell.x = p.x | (p.y << 11) | (p.z << 22);
        cell.y = 0u;  // Unused
        cell.z = glm::packUnorm4x8(glm::vec4(c[0], c[1], c[2], c[3]) / 255.0f);
        cell.w = glm::packUnorm4x8(glm::vec4(c[4], c[5], c[6], c[7]) / 255.0f);
        buffer.push_back(cell);
    }

    while (buffer.size() % 64) {
        // Pad with empty cells to keep cluster within brick
        buffer.push_back(glm::uvec4(0u));
    }

    // Shuffle extracted cells, so that drawing hidden cells at a lower rate
    // will still provide a good sampling of the brick
    std::shuffle(buffer.begin(), buffer.end(), std::default_random_engine(1234));

    // Append result to cell buffer, and output the count for brick
    for (auto &cell : buffer) cells.cells.push_back(cell);
    return uint32_t(buffer.size());
}

void extractSDFCells(Context &ctx)
{
    ctx.cells.cells.clear();
    ctx.cells.bricks.clear();
    ctx.cells.lodinfo.clear();

    const int32_t num_levels = int32_t(ctx.mipmap.size());
    ctx.cells.lodinfo.resize(num_levels);

    for (int32_t level = num_levels - 1; level >= 0; --level) {
        const cg::VolumeUInt8 &volume = ctx.mipmap[level];
        const glm::ivec3 grid_dim = volume.base.dimensions;
        const glm::ivec3 brick_dim = glm::ivec3(16);
        const glm::ivec3 num_bricks = glm::max(glm::ivec3(1), grid_dim / brick_dim);

        for (int32_t k = 0; k < num_bricks.z; ++k) {
            for (int32_t j = 0; j < num_bricks.y; ++j) {
                for (int32_t i = 0; i < num_bricks.x; ++i) {
                    const int32_t xmin = (i + 0) * brick_dim.x;
                    const int32_t xmax = (i + 1) * brick_dim.x;
                    const int32_t ymin = (j + 0) * brick_dim.y;
                    const int32_t ymax = (j + 1) * brick_dim.y;
                    const int32_t zmin = (k + 0) * brick_dim.z;
                    const int32_t zmax = (k + 1) * brick_dim.z;

                    const size_t first = ctx.cells.cells.size();
                    computeBrick(ctx.mipmap, level, xmin, xmax, ymin, ymax, zmin, zmax, ctx.cells);
                    const size_t count = ctx.cells.cells.size() - first;
                    if (count > 0) {
                        const int32_t xmid = (xmin + xmax) / 2;
                        const int32_t ymid = (ymin + ymax) / 2;
                        const int32_t zmid = (zmin + zmax) / 2;
                        ctx.cells.bricks.push_back(glm::uvec4(first / CLUSTER_SIZE,
                                                              count / CLUSTER_SIZE, level,
                                                              xmid | (ymid << 8) | (zmid << 16)));
                    }
                }
            }
        }
        ctx.cells.lodinfo[level] = glm::uvec4(grid_dim, ctx.cells.bricks.size());
    }

    createSDFCellBuffer(&ctx.buffers["cells"], &ctx.textures["cells"], ctx.cells.cells);
    createSDFCellBuffer(&ctx.buffers["bricks"], &ctx.textures["bricks"], ctx.cells.bricks);
    createSDFCellBuffer(&ctx.buffers["lodinfo"], &ctx.textures["lodinfo"], ctx.cells.lodinfo);
    createVisibilityBuffer(&ctx.buffers["visibility"], &ctx.textures["visibility"]);
    createVisibilityBuffer(&ctx.buffers["visibility2"], &ctx.textures["visibility2"]);
    createIndirectBuffer(&ctx.buffers["indirect"], &ctx.textures["indirect"]);
}

void computeVolumeMipmap(const cg::VolumeUInt8 &volume, std::vector<cg::VolumeUInt8> &mipmap)
{
    mipmap.resize(5);
    mipmap[0] = volume;  // Fill the first mipmap level
    for (int32_t level = 1; level < 5; ++level) {
        cg::VolumeUInt8 &src = mipmap[level - 1];
        cg::VolumeUInt8 &dst = mipmap[level];

        dst.base.dimensions = src.base.dimensions / 2;  // HACK
        dst.base.origin = src.base.origin;
        dst.base.spacing = src.base.spacing * 2.0f;
        dst.base.datatype = src.base.datatype;
        dst.base.data.resize(src.base.data.size() / 8);  // HACK

        for (int32_t z = 0; z < dst.base.dimensions.z; ++z) {
            for (int32_t y = 0; y < dst.base.dimensions.y; ++y) {
                for (int32_t x = 0; x < dst.base.dimensions.x; ++x) {
#if 0
                    const float value = float(cg::volumeInterpolate(
                        src, float(x * 2 + 1), float(y * 2 + 1), float(z * 2 + 1)));
#else
                    const float value = float(cg::volumeMinDist(
                        src, float(x * 2 + 1), float(y * 2 + 1), float(z * 2 + 1)));
#endif
                    dst(x, y, z) =
                        uint8_t(glm::max(0.0f, glm::min(255.0f, (value - 128.0f) * 0.5f + 128.0f)));
                }
            }
        }
    }
}

void updateDrawableInstances(Context &ctx)
{
    // Calculate base transform for instanced object
    const glm::vec3 extent = glm::vec3(ctx.mipmap[0].base.dimensions);
    const glm::vec3 aspect = extent / glm::max(extent.x, glm::max(extent.y, extent.z));
    const glm::mat4 world_from_local = glm::translate(glm::mat4(1.0f), -0.5f * aspect) *
                                       glm::scale(glm::mat4(1.0f), 1.0f * aspect / extent);

    // Get parameters for generation of instances
    const glm::ivec3 grid_dim = ctx.drawable_instances.grid_dim;
    assert(glm::compMax(grid_dim) > 0);
    const glm::vec3 grid_scale = 1.0f / glm::vec3(float(glm::compMax(grid_dim)));
    const glm::vec3 grid_center = glm::vec3(grid_dim) * 0.5f;

    // Calculate unique transform per instance
    std::srand(4321);  // Want this part to be deterministic
    ctx.drawable_instances.world_from_local.clear();
    for (int32_t z = 0; z < grid_dim.z; ++z) {
        for (int32_t y = 0; y < grid_dim.y; ++y) {
            for (int32_t x = 0; x < grid_dim.x; ++x) {
                const glm::vec3 grid_pos = (glm::vec3(x, y, z) + 0.5f - grid_center) * grid_scale;
                const glm::vec3 axis = glm::sphericalRand(1.0f);
                const float angle = glm::linearRand(-1.0f, 1.0f) * 3.141592f;
                const glm::mat4 transform =
                    glm::translate(glm::mat4(1.0f), grid_pos) *
                    glm::scale(glm::mat4(1.0f), grid_scale) *
                    glm::rotate(glm::mat4(1.0f), angle, axis) *
                    glm::scale(glm::mat4(1.0f), glm::vec3(glm::linearRand(0.5f, 1.5f))) *
                    world_from_local;
                ctx.drawable_instances.world_from_local.push_back(transform);
            }
        }
    }

    // Update storage buffer for instance data
    const uint32_t num_instances = uint32_t(ctx.drawable_instances.world_from_local.size());
    assert(num_instances <= MAX_NUM_INSTANCES);
    const size_t num_bytes = num_instances * sizeof(glm::mat4);
    glNamedBufferSubData(ctx.buffers["drawable_instances"], 0, num_bytes,
                         &ctx.drawable_instances.world_from_local[0]);

    // Create indirect draw commands for instances
    ctx.drawable_instances.indirect_cmds.clear();
    for (uint32_t i = 0; i < num_instances; ++i) {
        const uint32_t count = uint32_t(ctx.cells.bricks.size());
        const uint32_t base_instance = i;
        const glm::uvec4 cmd = glm::uvec4(count, 1, 0, base_instance);
        ctx.drawable_instances.indirect_cmds.push_back(cmd);
    }
    glNamedBufferSubData(ctx.buffers["indirect"], 0, num_instances * sizeof(glm::uvec4),
                         &ctx.drawable_instances.indirect_cmds[0]);
}

void loadVolume(Context &ctx)
{
    if (ctx.settings.input_filename != "")
        cg::volumeLoadVTK((cg::VolumeBase *)&ctx.volume, ctx.settings.input_filename);
    glm::ivec3 dim = ctx.volume.base.dimensions;
    std::fprintf(stdout, "Input volume size: %dx%dx%d\n", dim.x, dim.y, dim.z);

    // Generate mipmap levels
    double tic0 = glfwGetTime();
    computeVolumeMipmap(ctx.volume, ctx.mipmap);
    std::fprintf(stdout, "Mipmap calculation time (s): %lf\n", glfwGetTime() - tic0);

    // Extract grid cell data structure
    double tic1 = glfwGetTime();
    extractSDFCells(ctx);
    std::fprintf(stdout, "Cell extraction time (s): %lf\n", glfwGetTime() - tic1);

    // Update instance transforms
    updateDrawableInstances(ctx);
}

void doInitialization(Context &ctx)
{
    // Initialize GL objects
    setupGLPrograms(ctx);
    setupGLPipelines(ctx);
    setupGLFramebuffers(ctx);
    setupGLBuffers(ctx);
    setupGLQueries(ctx);

    // Setup trackball and viewing
    setupTrackball(ctx);
    setupCameras(ctx);

    // Load input model
    loadVolume(ctx);
}

void dispatchDeferred(Context &ctx)
{
    glBindBufferBase(GL_UNIFORM_BUFFER, 0, ctx.buffers["scene_uniforms"]);
    glBindTextureUnit(2, ctx.textures[ctx.settings.use_atomic_splatting ? "depth2" : "depth"]);
    glBindImageTexture(1, ctx.textures["gbuffer"], 0, GL_FALSE, 0, GL_READ_ONLY, GL_RGBA16F);
    glBindImageTexture(2, ctx.textures["id"], 0, GL_FALSE, 0, GL_READ_ONLY, GL_RG32UI);
    glBindImageTexture(5, ctx.textures["color"], 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_RGBA16F);

    glUseProgram(ctx.programs["deferred_cs"]);
    {
        glUniform3fv(0, 1, &ctx.settings.bg_color0[0]);
        glUniform3fv(1, 1, &ctx.settings.bg_color1[0]);
        glUniform3fv(2, 1, &ctx.settings.base_color[0]);
        glUniform1f(3, ctx.settings.roughness);
        glUniform1i(4, ctx.settings.show_normals);
        glUniform1i(5, ctx.settings.show_visibility_id);
        glUniform1i(6, ctx.settings.show_cell_id);
        glUniform1i(7, ctx.settings.show_lod);
        glUniform1i(8, ctx.settings.gamma_enabled);
        glDispatchCompute(ctx.window.width / 8 + 1, ctx.window.height / 8 + 1, 1);
        glMemoryBarrier(GL_ALL_BARRIER_BITS);
    }
    glUseProgram(0);
}

void dispatchVisibility(Context &ctx)
{
    if (ctx.settings.use_atomic_splatting && ctx.settings.fill_holes &&
        !ctx.settings.use_indirect_draw)
        return;

    glBindTextureUnit(0, ctx.textures["id"]);
    glBindImageTexture(0, ctx.textures["visibility"], 0, GL_FALSE, 0, GL_READ_WRITE, GL_R32UI);
    glClearNamedBufferData(ctx.buffers["visibility"], GL_R32UI, GL_RED_INTEGER, GL_UNSIGNED_INT,
                           nullptr);
    if (!ctx.settings.use_atomic_splatting)
        glClearNamedBufferData(ctx.buffers["visibility2"], GL_R32UI, GL_RED_INTEGER,
                               GL_UNSIGNED_INT, nullptr);

    glUseProgram(ctx.programs["visibility_cs"]);
    {
        glDispatchCompute(ctx.window.width / 8, ctx.window.height / 8, 1);
        glMemoryBarrier(GL_ALL_BARRIER_BITS);
    }
    glUseProgram(0);
}

void drawSDFCellsWithTesselation(Context &ctx)
{
    glBindBufferBase(GL_UNIFORM_BUFFER, 0, ctx.buffers["scene_uniforms"]);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, ctx.buffers["drawable_instances"]);
    glBindBuffer(GL_DRAW_INDIRECT_BUFFER, ctx.buffers["indirect"]);
    glBindTextureUnit(0, ctx.textures["cells"]);
    glBindTextureUnit(1, ctx.textures["bricks"]);
    glBindTextureUnit(2, ctx.textures["lodinfo"]);
    glBindTextureUnit(3, ctx.textures["depth"]);
    glBindImageTexture(0, ctx.textures["visibility"], 0, GL_FALSE, 0, GL_READ_ONLY, GL_R32UI);
    glBindImageTexture(1, ctx.textures["visibility2"], 0, GL_FALSE, 0, GL_READ_WRITE, GL_R32UI);
    glBindImageTexture(2, ctx.textures["indirect"], 0, GL_FALSE, 0, GL_READ_WRITE, GL_RGBA32UI);

    const uint32_t num_instances = uint32_t(ctx.drawable_instances.world_from_local.size());
    assert(num_instances <= MAX_NUM_INSTANCES);

    {
        const GLenum draw_buffers[] = {GL_COLOR_ATTACHMENT3, GL_COLOR_ATTACHMENT4};
        glDrawBuffers(2, draw_buffers);

        glUsePipelineState(ctx.pipelines["draw_cells_visible_ts"]);
        {
            glUniform1i(0, ctx.settings.point_shape);
            glUniform1f(1, ctx.settings.point_scale);
            glUniform1f(2, float(ctx.settings.lod_threshold_pixels));
            glUniform1i(3, ctx.settings.freeze_visibility);
            glViewport(0, 0, ctx.window.width, ctx.window.height);
            glPatchParameteri(GL_PATCH_VERTICES, 1);
            if (ctx.settings.use_indirect_draw)
                glMultiDrawArraysIndirect(GL_PATCHES, nullptr, num_instances, 0);
            else
                glDrawArraysInstanced(GL_PATCHES, 0, uint32_t(ctx.cells.bricks.size()),
                                      num_instances);
        }
        glClearCurrentPipelineState();
    }

    if (!ctx.settings.freeze_visibility) {
        glUseProgram(ctx.programs["update_indirect_cs"]);
        {
            glUniform1f(0, float(ctx.settings.lod_threshold_pixels));
            if (ctx.settings.use_indirect_draw) {
                glDispatchCompute(num_instances / 64 + 1, 1, 1);  // FIXME
                glMemoryBarrier(GL_ALL_BARRIER_BITS);
            }
        }
        glUseProgram(0);
    }

    if (!ctx.settings.freeze_visibility) {
        const GLenum draw_buffers[] = {GL_NONE, GL_NONE};
        glDrawBuffers(2, draw_buffers);

        glUsePipelineState(ctx.pipelines["draw_cells_hidden_ts"]);
        {
            glUniform1i(0, ctx.settings.point_shape);
            glUniform1f(1, ctx.settings.point_scale);
            glUniform1f(2, float(ctx.settings.lod_threshold_pixels));
            glUniform1i(3, ctx.settings.freeze_visibility);
            glViewport(0, 0, ctx.window.width, ctx.window.height);
            glPatchParameteri(GL_PATCH_VERTICES, 1);
            if (ctx.settings.use_indirect_draw)
                glMultiDrawArraysIndirect(GL_PATCHES, nullptr, num_instances, 0);
            else
                glDrawArraysInstanced(GL_PATCHES, 0, uint32_t(ctx.cells.bricks.size()),
                                      num_instances);
            glMemoryBarrier(GL_ALL_BARRIER_BITS);
        }
        glClearCurrentPipelineState();
    }

    if (ctx.settings.fill_holes && !ctx.settings.freeze_visibility) {
        // Swap bindings of visibility mask buffers
        glBindImageTexture(1, ctx.textures["visibility"], 0, GL_FALSE, 0, GL_READ_WRITE, GL_R32UI);
        glBindImageTexture(0, ctx.textures["visibility2"], 0, GL_FALSE, 0, GL_READ_WRITE, GL_R32UI);

        const GLenum draw_buffers[] = {GL_COLOR_ATTACHMENT3, GL_COLOR_ATTACHMENT4};
        glDrawBuffers(2, draw_buffers);

        glUsePipelineState(ctx.pipelines["draw_cells_visible_ts"]);
        {
            glUniform1i(0, ctx.settings.point_shape);
            glUniform1f(1, ctx.settings.point_scale);
            glUniform1f(2, float(ctx.settings.lod_threshold_pixels));
            glUniform1i(3, ctx.settings.freeze_visibility);
            glViewport(0, 0, ctx.window.width, ctx.window.height);
            glPatchParameteri(GL_PATCH_VERTICES, 1);
            if (ctx.settings.use_indirect_draw)
                glMultiDrawArraysIndirect(GL_PATCHES, nullptr, num_instances, 0);
            else
                glDrawArraysInstanced(GL_PATCHES, 0, uint32_t(ctx.cells.bricks.size()),
                                      num_instances);
        }
        glClearCurrentPipelineState();

        glBindImageTexture(0, ctx.textures["visibility"], 0, GL_FALSE, 0, GL_READ_WRITE, GL_R32UI);
        glBindImageTexture(1, ctx.textures["visibility2"], 0, GL_FALSE, 0, GL_READ_WRITE, GL_R32UI);
    }

    if (ctx.settings.blend_normals) {
        const GLenum draw_buffers[] = {GL_COLOR_ATTACHMENT3, GL_NONE};
        glDrawBuffers(2, draw_buffers);

        glUsePipelineState(ctx.pipelines["draw_cells_ewa_ts"]);
        {
            glUniform1i(0, ctx.settings.point_shape);
            glUniform1f(1, ctx.settings.point_scale * 1.25f);
            glUniform1f(2, float(ctx.settings.lod_threshold_pixels));
            glUniform1i(3, ctx.settings.freeze_visibility);
            glViewport(0, 0, ctx.window.width, ctx.window.height);
            glPatchParameteri(GL_PATCH_VERTICES, 1);
            if (ctx.settings.use_indirect_draw)
                glMultiDrawArraysIndirect(GL_PATCHES, nullptr, num_instances, 0);
            else
                glDrawArraysInstanced(GL_PATCHES, 0, uint32_t(ctx.cells.bricks.size()), num_instances);
        }
        glClearCurrentPipelineState();
    }

    glDrawBuffer(GL_BACK);
}

void drawSDFCellsWithAtomics(Context &ctx)
{
    const uint32_t num_instances = uint32_t(ctx.drawable_instances.world_from_local.size());
    assert(num_instances <= MAX_NUM_INSTANCES);

    glBindBufferBase(GL_UNIFORM_BUFFER, 0, ctx.buffers["scene_uniforms"]);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, ctx.buffers["drawable_instances"]);
    glBindBuffer(GL_DRAW_INDIRECT_BUFFER, ctx.buffers["indirect"]);
    glBindTextureUnit(0, ctx.textures["cells"]);
    glBindTextureUnit(1, ctx.textures["bricks"]);
    glBindTextureUnit(2, ctx.textures["lodinfo"]);
    glBindImageTexture(0, ctx.textures["visibility"], 0, GL_FALSE, 0, GL_READ_ONLY, GL_R32UI);
    glBindImageTexture(1, ctx.textures["depth2"], 0, GL_FALSE, 0, GL_READ_WRITE, GL_R32UI);
    glBindImageTexture(2, ctx.textures["indirect"], 0, GL_FALSE, 0, GL_READ_WRITE, GL_RGBA32UI);
    glBindImageTexture(3, ctx.textures["gbuffer"], 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_RGBA16F);
    glBindImageTexture(4, ctx.textures["id"], 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_RG32UI);

    glDrawBuffer(GL_NONE);  // Disable writing to color attachments

    if (ctx.settings.use_indirect_draw) {
        glUsePipelineState(ctx.pipelines["draw_cells_atomic_fs"]);
        {
            glUniform1i(0, ctx.settings.point_shape);
            glUniform1f(1, ctx.settings.point_scale);
            glUniform1f(2, float(ctx.settings.lod_threshold_pixels));
            glUniform1i(3, ctx.settings.freeze_visibility);
            glUniform1i(4, 0);
            glViewport(0, 0, ctx.window.width, ctx.window.height);
            glMultiDrawArraysIndirect(GL_POINTS, nullptr, num_instances, 0);
            glMemoryBarrier(GL_ALL_BARRIER_BITS);
        }
        glClearCurrentPipelineState();

        if (!ctx.settings.freeze_visibility) {
            glUseProgram(ctx.programs["update_indirect_cs"]);
            {
                glUniform1f(0, float(ctx.settings.lod_threshold_pixels));
                glDispatchCompute(num_instances / 64 + 1, 1, 1);  // FIXME
                glMemoryBarrier(GL_ALL_BARRIER_BITS);
            }
            glUseProgram(0);
        }

        glUsePipelineState(ctx.pipelines["draw_cells_atomic_fs"]);
        {
            glUniform1i(0, ctx.settings.point_shape);
            glUniform1f(1, ctx.settings.point_scale);
            glUniform1f(2, float(ctx.settings.lod_threshold_pixels));
            glUniform1i(3, ctx.settings.freeze_visibility);
            glUniform1i(4, 1);
            glViewport(0, 0, ctx.window.width, ctx.window.height);
            glMultiDrawArraysIndirect(GL_POINTS, nullptr, num_instances, 0);
            glMemoryBarrier(GL_ALL_BARRIER_BITS);
        }
        glClearCurrentPipelineState();
    } else {
        if (ctx.settings.fill_holes) {
            glUsePipelineState(ctx.pipelines["draw_cells_atomic_ts2"]);
            {
                glUniform1i(0, ctx.settings.point_shape);
                glUniform1f(1, ctx.settings.point_scale);
                glUniform1f(2, float(ctx.settings.lod_threshold_pixels));
                glUniform1i(3, ctx.settings.freeze_visibility);
                glViewport(0, 0, ctx.window.width, ctx.window.height);
                glPatchParameteri(GL_PATCH_VERTICES, 1);
                for (int op = 0; op <= 2; ++op) {
                    glUniform1i(4, op);
                    glDrawArrays(GL_PATCHES, 0, num_instances);
                    glMemoryBarrier(GL_ALL_BARRIER_BITS);
                }
            }
            glClearCurrentPipelineState();
        } else {
            glUsePipelineState(ctx.pipelines["draw_cells_atomic_ts"]);
            {
                glUniform1i(0, ctx.settings.point_shape);
                glUniform1f(1, ctx.settings.point_scale);
                glUniform1f(2, float(ctx.settings.lod_threshold_pixels));
                glUniform1i(3, ctx.settings.freeze_visibility);
                glViewport(0, 0, ctx.window.width, ctx.window.height);
                glPatchParameteri(GL_PATCH_VERTICES, 1);
                for (int op = 0; op <= 1; ++op) {
                    glUniform1i(4, op);
                    glDrawArrays(GL_PATCHES, 0, num_instances);
                    glMemoryBarrier(GL_ALL_BARRIER_BITS);
                }
            }
            glClearCurrentPipelineState();
        }
    }

    glDrawBuffer(GL_BACK);
}

void drawFxaaGamma(Context &ctx)
{
    glBindTextureUnit(0, ctx.textures["color"]);

    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    glDrawBuffer(GL_BACK);
    glUsePipelineState(ctx.pipelines["fxaa_gamma"]);
    {
        glViewport(0, 0, ctx.window.width, ctx.window.height);
        glUniform1i(0, ctx.settings.fxaa_enabled);
        glUniform1i(1, ctx.settings.gamma_enabled);
        glDrawArrays(GL_TRIANGLES, 0, 3);
    }
    glClearCurrentPipelineState();
}

void doRendering(Context &ctx)
{
    if (ctx.window.idle_frames < 100 || ctx.window.update_when_idle) {
        // Clear timers
        for (auto &ti : ctx.timers) { ti.second.result = 0; }

        // Update per-frame uniforms
        glNamedBufferSubData(ctx.buffers["scene_uniforms"], 0, sizeof(ctx.scene_uniforms),
                             &ctx.scene_uniforms);

        // Clear framebuffers/textures
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, ctx.framebuffers["forward"]);
        glDrawBuffer(GL_COLOR_ATTACHMENT5);  // 2nd depth texture
        glClearBufferfv(GL_COLOR, 0, &glm::vec4(1.0f)[0]);
        glDrawBuffer(GL_COLOR_ATTACHMENT4);  // ID texture
        glClearBufferfv(GL_COLOR, 0, &glm::vec4(0.0f)[0]);
        glDrawBuffer(GL_COLOR_ATTACHMENT3);  // G-buffer
        glClearBufferfv(GL_COLOR, 0, &glm::vec4(0.0f)[0]);
        glDrawBuffer(GL_COLOR_ATTACHMENT0);  // Color
        glClearBufferfv(GL_COLOR, 0, &glm::vec4(0.0f)[0]);
        glClearBufferfi(GL_DEPTH_STENCIL, 0, 1.0f, 0);

        glBeginQuery(GL_TIME_ELAPSED, ctx.timers["rasterise"].query);
        if (ctx.settings.use_atomic_splatting) {
            drawSDFCellsWithAtomics(ctx);
        } else {
            drawSDFCellsWithTesselation(ctx);
        }
        glEndQuery(GL_TIME_ELAPSED);
        glGetQueryObjectui64v(ctx.timers["rasterise"].query, GL_QUERY_RESULT,
                              &ctx.timers["rasterise"].result);

        if (!ctx.settings.freeze_visibility) {
            glBeginQuery(GL_TIME_ELAPSED, ctx.timers["visibility"].query);
            dispatchVisibility(ctx);
            glEndQuery(GL_TIME_ELAPSED);
            glGetQueryObjectui64v(ctx.timers["visibility"].query, GL_QUERY_RESULT,
                                  &ctx.timers["visibility"].result);
        }

        glBeginQuery(GL_TIME_ELAPSED, ctx.timers["deferred"].query);
        dispatchDeferred(ctx);
        glEndQuery(GL_TIME_ELAPSED);
        glGetQueryObjectui64v(ctx.timers["deferred"].query, GL_QUERY_RESULT,
                              &ctx.timers["deferred"].result);
    }

    glBeginQuery(GL_TIME_ELAPSED, ctx.timers["fxaa_gamma"].query);
    drawFxaaGamma(ctx);
    glEndQuery(GL_TIME_ELAPSED);
    glGetQueryObjectui64v(ctx.timers["fxaa_gamma"].query, GL_QUERY_RESULT,
                          &ctx.timers["fxaa_gamma"].result);
}

void showSettingsWindow(Context &ctx)
{
    ImGui::SetNextWindowBgAlpha(0.35f);
    ImGui::SetNextWindowCollapsed(true, ImGuiCond_Once);
    if (ImGui::Begin("Settings")) {
        if (ImGui::CollapsingHeader("Rasterisation")) {
            ImGui::Checkbox("Show normals", (bool *)&ctx.settings.show_normals);
            ImGui::Checkbox("Show visibility ID", (bool *)&ctx.settings.show_visibility_id);
            ImGui::Checkbox("Show cell ID", (bool *)&ctx.settings.show_cell_id);
            ImGui::Checkbox("Show LOD", (bool *)&ctx.settings.show_lod);
            ImGui::Checkbox("Fill holes", (bool *)&ctx.settings.fill_holes);
            ImGui::Checkbox("Blend normals", (bool *)&ctx.settings.blend_normals);
            ImGui::Checkbox("Use indirect draw", (bool *)&ctx.settings.use_indirect_draw);
            ImGui::Checkbox("Use atomic splatting", (bool *)&ctx.settings.use_atomic_splatting);
            ImGui::Checkbox("Freeze visibility", (bool *)&ctx.settings.freeze_visibility);
            ImGui::Combo("Point shape", (int *)&ctx.settings.point_shape,
                         "Square\0Round\0Disk\0Box\0Cell\0");
            ImGui::SliderFloat("Point scale", &ctx.settings.point_scale, 0.1f, 3.0f, "%.1f");
            ImGui::SliderInt("LOD threshold", &ctx.settings.lod_threshold_pixels, 1, 8);
        }
        if (ImGui::CollapsingHeader("Scene")) {
            ImGui::ColorEdit3("BG color 0", &ctx.settings.bg_color0[0]);
            ImGui::ColorEdit3("BG color 1", &ctx.settings.bg_color1[0]);
            ImGui::ColorEdit3("Base color", &ctx.settings.base_color[0]);
            ImGui::SliderFloat("Roughness", &ctx.settings.roughness, 0.0f, 1.0f);
            if (ImGui::SliderInt3("Instances", &ctx.drawable_instances.grid_dim[0], 1, 32))
                updateDrawableInstances(ctx);
        }
        if (ImGui::CollapsingHeader("Misc")) {
            ImGui::Checkbox("FXAA enabled", (bool *)&ctx.settings.fxaa_enabled);
            ImGui::Checkbox("Gamma enabled", (bool *)&ctx.settings.gamma_enabled);
            ImGui::Checkbox("Update when idle", (bool *)&ctx.window.update_when_idle);
            if (ImGui::Checkbox("Vsync enabled", (bool *)&ctx.window.swap_interval))
                glfwSwapInterval(ctx.window.swap_interval & 1);
        }
    }
    ImGui::End();
}

void showPerformanceWindow(Context &ctx)
{
    ImGui::SetNextWindowBgAlpha(0.35f);
    ImGui::SetNextWindowCollapsed(true, ImGuiCond_Once);
    if (ImGui::Begin("Performance")) {
        ImGui::Text("Frame rate: %.1f", ImGui::GetIO().Framerate);
        ImGui::Text("Frame times (GPU):");
        {
            int i = 0;
            double t0 = 0.0, t1 = 0.0;
            for (auto &timer : ctx.timers) {
                t0 += (t1 = (double(timer.second.result) / 1e6));
                ImGui::PushStyleColor(ImGuiCol_Button,
                                      ImColor::HSV((i++) / 7.0f, 0.6f, 0.6f).Value);
                ImGui::Button("", ImVec2(float(t1 * (ImGui::GetWindowWidth() / 16.7)), 8));
                if (ImGui::IsItemHovered()) {
                    ImGui::SetTooltip("%s: %.1lf (ms)", timer.first.c_str(), t1);
                }
                ImGui::PopStyleColor();
            }
            ImGui::PushStyleColor(ImGuiCol_Button, ImColor::HSV(i / 7.0f, 0.6f, 0.6f).Value);
            ImGui::Button("", ImVec2(float(t0 * (ImGui::GetWindowWidth() / 16.7)), 8));
            if (ImGui::IsItemHovered()) { ImGui::SetTooltip("total: %.1lf (ms)", t0); }
            ImGui::PopStyleColor();
        }
        int i = 0;
        for (auto &timer : ctx.timers) {
            ImGui::TextColored(ImColor::HSV((i++) / 7.0f, 0.6f, 0.6f), "%s", timer.first.c_str());
            ImGui::SameLine();
        }
        ImGui::TextColored(ImColor::HSV(i / 7.0f, 0.6f, 0.6f), "total");

        ImGui::Separator();

        ImGui::Text("Cell count: %lu", ctx.cells.cells.size());
        ImGui::Text("Cluster count: %lu", ctx.cells.cells.size() / CLUSTER_SIZE);
        ImGui::Text("Brick count: %lu", ctx.cells.bricks.size());
        ImGui::Text("Instance count: %lu", ctx.drawable_instances.world_from_local.size());
        ImGui::Text("Resolution: %dx%d", ctx.window.width, ctx.window.height);
    }
    ImGui::End();
}

void doUpdate(Context &ctx)
{
    if (ctx.window.idle_frames < 100 || ctx.window.update_when_idle) {
        // Update viewing
        glm::mat4 trackball = cg::cgTrackballGetRotationMatrix(ctx.interactor.trackball);
        ctx.camera.view_from_world = glm::translate(glm::mat4(1.0f), -ctx.camera.location);
        ctx.camera.view_from_world = ctx.camera.view_from_world * trackball;

        // Update per-frame uniforms
        ctx.scene_uniforms.proj_from_view = ctx.camera.proj_from_view;
        ctx.scene_uniforms.view_from_world = ctx.camera.view_from_world;
        ctx.scene_uniforms.resolution[0] = float(ctx.window.width);
        ctx.scene_uniforms.resolution[1] = float(ctx.window.height);
        ctx.scene_uniforms.time = ctx.window.elapsed_time;
        ctx.scene_uniforms.frame += 1;
    }

    // Show GUI
    showSettingsWindow(ctx);
    showPerformanceWindow(ctx);
}

void errorCallback(int /*error*/, const char *description)
{
    std::fprintf(stderr, "%s\n", description);
}

void mouseButtonCallback(GLFWwindow *window, int button, int action, int mods)
{
    ImGui_ImplGlfw_MouseButtonCallback(window, button, action, mods);
    if (ImGui::GetIO().WantCaptureMouse) { return; }  // Skip other handling

    double x, y;
    glfwGetCursorPos(window, &x, &y);

    Context *ctx = static_cast<Context *>(glfwGetWindowUserPointer(window));
    ctx->window.idle_frames = 0;
    if (button == GLFW_MOUSE_BUTTON_RIGHT) {
        if (action == GLFW_PRESS) {
            ctx->interactor.trackball.center = {x, y};
            cg::cgTrackballStartTracking(ctx->interactor.trackball, {x, y});
        }
        if (action == GLFW_RELEASE) { cg::cgTrackballStopTracking(ctx->interactor.trackball); }
    }
    if (button == GLFW_MOUSE_BUTTON_MIDDLE) {
        ctx->interactor.panning = (action == GLFW_PRESS);
        ctx->interactor.pan_center = {x, y};
    }
}

void scrollCallback(GLFWwindow *window, double /*xoffset*/, double yoffset)
{
    if (ImGui::GetIO().WantCaptureMouse) { return; }  // Skip other handling

    Context *ctx = static_cast<Context *>(glfwGetWindowUserPointer(window));
    ctx->window.idle_frames = 0;
    ctx->camera.location.z += 0.05f * glm::sign(float(yoffset));
    ctx->camera.location.z = glm::clamp(ctx->camera.location.z, 0.5f, 10.0f);
}

void cursorPosCallback(GLFWwindow *window, double x, double y)
{
    if (ImGui::GetIO().WantCaptureMouse) { return; }  // Skip other handling

    Context *ctx = static_cast<Context *>(glfwGetWindowUserPointer(window));
    if (ctx->interactor.trackball.tracking) {
        cg::cgTrackballMove(ctx->interactor.trackball, {x, y});
        ctx->window.idle_frames = 0;
    }
    if (ctx->interactor.panning) {
        ctx->camera.location.x -= (float(x) - ctx->interactor.pan_center.x) * 0.001f;
        ctx->camera.location.y += (float(y) - ctx->interactor.pan_center.y) * 0.001f;
        ctx->interactor.pan_center = {x, y};
        ctx->window.idle_frames = 0;
    }
}

void keyCallback(GLFWwindow *window, int key, int scancode, int action, int mods)
{
    ImGui_ImplGlfw_KeyCallback(window, key, scancode, action, mods);
    if (ImGui::GetIO().WantCaptureKeyboard) { return; }  // Skip other handling

    Context *ctx = static_cast<Context *>(glfwGetWindowUserPointer(window));
    if (key == GLFW_KEY_F5 && action == GLFW_PRESS) {
        setupGLPrograms(*ctx);
        setupGLPipelines(*ctx);
        ctx->window.idle_frames = 0;
    }
}

void dropCallback(GLFWwindow *window, int count, const char **paths)
{
    Context *ctx = static_cast<Context *>(glfwGetWindowUserPointer(window));
    if (count > 0) {
        ctx->settings.input_filename = std::string(paths[0]);
        loadVolume(*ctx);
    }
}

void resizeCallback(GLFWwindow *window, int width, int height)
{
    Context *ctx = static_cast<Context *>(glfwGetWindowUserPointer(window));
    ctx->window.width = width;
    ctx->window.height = height;
    ctx->window.idle_frames = 0;

    setupGLFramebuffers(*ctx);
    setupTrackball(*ctx);
    setupCameras(*ctx);
}

int main(int argc, char *argv[])
{
    // Create application context (for passing around)
    Context ctx;
    if (argc > 1) { ctx.settings.input_filename = std::string(argv[1]); }

    // Create window and GL context
    glfwInit();
    glfwSetErrorCallback(errorCallback);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#if 0
    glfwWindowHint(GLFW_SAMPLES, 4);
#endif
    ctx.window.id = glfwCreateWindow(ctx.window.width, ctx.window.height, "sdf_cells", 0, 0);
    if (!ctx.window.id) { std::exit(EXIT_FAILURE); }
    glfwMakeContextCurrent(ctx.window.id);
    glfwSetWindowUserPointer(ctx.window.id, &ctx);
    glfwSetMouseButtonCallback(ctx.window.id, mouseButtonCallback);
    glfwSetScrollCallback(ctx.window.id, scrollCallback);
    glfwSetCursorPosCallback(ctx.window.id, cursorPosCallback);
    glfwSetKeyCallback(ctx.window.id, keyCallback);
    glfwSetFramebufferSizeCallback(ctx.window.id, resizeCallback);
    glfwSetDropCallback(ctx.window.id, dropCallback);
    glfwSwapInterval(ctx.window.swap_interval);

    // Load GL functions
    if (gl3wInit() || !gl3wIsSupported(4, 5) /*check OpenGL version*/) {
        std::fprintf(stderr, "Error: failed to initialize OpenGL\n");
        std::exit(EXIT_FAILURE);
    }
    std::fprintf(stdout, "GL version: %s\n", glGetString(GL_VERSION));

    // Create default VAO and bind it once here
    glCreateVertexArrays(1, &ctx.vaos["default"]);
    glBindVertexArray(ctx.vaos["default"]);
#if 1
    for (int32_t i = 0; i < 4; ++i) {
        float sample_pos[2];
        glGetMultisamplefv(GL_SAMPLE_POSITION, i, sample_pos);
        std::fprintf(stdout, "Sample position %d: %f %f\n", i, sample_pos[0], sample_pos[1]);
    }
    float point_size_granularity;
    glGetFloatv(GL_POINT_SIZE_GRANULARITY, &point_size_granularity);
    std::fprintf(stdout, "Point size granularity: %f\n", point_size_granularity);
#endif

    // Initialize ImGui (without installing default callbacks)
    ImGui::CreateContext();
    ImGui_ImplGlfw_InitForOpenGL(ctx.window.id, false);
    ImGui_ImplOpenGL3_Init("#version 450" /*GLSL version*/);

    // Initialize application
    doInitialization(ctx);

    // Start main loop
    while (!glfwWindowShouldClose(ctx.window.id)) {
        glfwPollEvents();
        ctx.window.elapsed_time = float(glfwGetTime());
        ctx.window.idle_frames += 1;

        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();
        doUpdate(ctx);
        doRendering(ctx);
        ImGui::Render();
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

        glfwSwapBuffers(ctx.window.id);
    }

    // Shutdown everything
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();
    glfwDestroyWindow(ctx.window.id);
    glfwTerminate();
    std::exit(EXIT_SUCCESS);
}
