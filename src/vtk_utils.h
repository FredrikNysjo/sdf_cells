#pragma once

#include <vector>
#include <string>
#include <cstdint>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace cg {

// Base struct for volume images
struct VolumeBase {
    glm::ivec3 dimensions;           // volume dimensions
    glm::vec3 origin;                // volume origin
    glm::vec3 spacing;               // voxel spacing
    std::string datatype;            // voxel data type string
    std::vector<std::uint8_t> data;  // voxel data
};

// Template struct for typed volume images
template <typename VoxelType> struct Volume {
    VolumeBase base;
    typedef VoxelType voxel_type;

    // Overridden operator for element access (no bounds checking!)
    VoxelType &operator()(int x, int y, int z);
    const VoxelType &operator()(int x, int y, int z) const;
};

// Typed volume images
typedef Volume<std::uint8_t> VolumeUInt8;
typedef Volume<std::uint16_t> VolumeUInt16;
typedef Volume<std::int16_t> VolumeInt16;
typedef Volume<std::uint32_t> VolumeUInt32;
typedef Volume<float> VolumeFloat32;

// Overridden operator for element access (no bounds checking!)
template <typename VoxelType> inline VoxelType &Volume<VoxelType>::operator()(int x, int y, int z)
{
    const glm::ivec3 &res = base.dimensions;
    const size_t idx = (size_t)res.x * res.y * z + res.x * y + x;
    return reinterpret_cast<VoxelType *>(&base.data[0])[idx];
}

template <typename VoxelType>
inline const VoxelType &Volume<VoxelType>::operator()(int x, int y, int z) const
{
    const glm::ivec3 &res = base.dimensions;
    const size_t idx = (size_t)res.x * res.y * z + res.x * y + x;
    return reinterpret_cast<const VoxelType *>(&base.data[0])[idx];
}

template <typename VoxelType>
inline float volumeInterpolate(const Volume<VoxelType> &volume, float x, float y, float z)
{
    const glm::ivec3 &dim = volume.base.dimensions;
    const glm::vec3 p = {x - 0.5f, y - 0.5f, z - 0.5f};
    const glm::ivec3 q = glm::ivec3(glm::floor(p));

    float c[8] = {0.0f};  // TODO: Make border value a parameter
    for (int32_t k = 0; k < 2; ++k)
        for (int32_t j = 0; j < 2; ++j)
            for (int32_t i = 0; i < 2; ++i)
                if ((q.z + k >= 0 && q.z + k < dim.z) &&
                    (q.y + j >= 0 && q.y + j < dim.y) &&
                    (q.x + i >= 0 && q.x + i < dim.x))
                    c[k * 4 + j * 2 + i] = volume(q.x + i, q.y + j, q.z + k);

    const glm::vec3 t = glm::fract(p);
    return glm::mix(glm::mix(glm::mix(c[0], c[1], t.x), glm::mix(c[2], c[3], t.x), t.y),
                    glm::mix(glm::mix(c[4], c[5], t.x), glm::mix(c[6], c[7], t.x), t.y), t.z);
}

template <typename VoxelType>
inline float volumeMax(const Volume<VoxelType> &volume, float x, float y, float z)
{
    const glm::ivec3 &dim = volume.base.dimensions;
    const glm::vec3 p = {x - 0.5f, y - 0.5f, z - 0.5f};
    const glm::ivec3 q = glm::ivec3(glm::floor(p));

    VoxelType max_value = VoxelType(0);
    for (int32_t k = 0; k < 2; ++k)
        for (int32_t j = 0; j < 2; ++j)
            for (int32_t i = 0; i < 2; ++i)
                if ((q.z + k >= 0 && q.z + k < dim.z) &&
                    (q.y + j >= 0 && q.y + j < dim.y) &&
                    (q.x + i >= 0 && q.x + i < dim.x))
                    max_value = glm::max(max_value, volume(q.x + i, q.y + j, q.z + k));

    return float(max_value);
}

template <typename VoxelType>
inline float volumeMinDist(const Volume<VoxelType> &volume, float x, float y, float z)
{
    const glm::ivec3 &dim = volume.base.dimensions;
    const glm::vec3 p = {x - 0.5f, y - 0.5f, z - 0.5f};
    const glm::ivec3 q = glm::ivec3(glm::floor(p));

    float min_dist = 99999.0f;
    for (int32_t k = 0; k < 2; ++k)
        for (int32_t j = 0; j < 2; ++j)
            for (int32_t i = 0; i < 2; ++i)
                if ((q.z + k >= 0 && q.z + k < dim.z) &&
                    (q.y + j >= 0 && q.y + j < dim.y) &&
                    (q.x + i >= 0 && q.x + i < dim.x)) {
                    float value = volume(q.x + i, q.y + j, q.z + k);
                    if (glm::abs(value - 128.0f) < glm::abs(min_dist - 128.0f))
                        min_dist = value;
                }

    return min_dist;
}

// Computes the extent (dimensions*spacing) of the volume image
inline glm::vec3 volumeComputeExtent(const VolumeBase &volume)
{
    return glm::vec3(volume.dimensions) * volume.spacing;
}

// Computes the model matrix for the volume image. This matrix can be
// used during rendering to scale a 2-unit cube to the size of
// the volume image. Assumes that the cube is centered at origin.
inline glm::mat4 volumeComputeModelMatrix(const VolumeBase &volume)
{
    glm::vec3 extent = volumeComputeExtent(volume);
    return glm::translate(glm::mat4(), volume.origin) * glm::scale(glm::mat4(), 0.5f * extent);
}

// Computes the bounding sphere of the volume image
inline glm::vec4 volumeComputeBSphere(const VolumeBase &volume)
{
    glm::vec3 extent = volumeComputeExtent(volume);
    float radius = 0.5f * glm::max(extent[0], glm::max(extent[1], extent[2]));
    glm::vec3 center = volume.origin;
    return glm::vec4(center, radius);
}

// Reads a volume image in the legacy VTK StructuredPoints format
// from a file. Returns true on success, false otherwise. Possible
// datatypes are: "uint8", "uint16", "int16", "uint32", and "float32".
// Assumes that the file starts with a ten line header
// section followed by a data section in ASCII or binary format,
// i.e.:
//
// # vtk DataFile Version x.x\n
// Some information about the file\n
// BINARY\n
// DATASET STRUCTURED_POINTS\n
// DIMENSIONS 128 128 128\n
// ORIGIN 0.0 0.0 0.0\n
// SPACING 1.0 1.0 1.0\n
// POINT_DATA 2097152\n
// SCALARS image_data unsigned_char\n
// LOOKUP_TABLE default\n
// raw data........\n
bool volumeLoadVTK(VolumeBase *volume, const std::string &filename);

// Types for polygonal datasets
enum PolyDataType {
    POLYDATA_TYPE_NONE = 0,
    POLYDATA_TYPE_POINTCLOUD_RGB = 1,
};

// Base struct for polygonal datasets
struct PolyDataBase {
    std::vector<glm::vec3> points;
    PolyDataType type;

    PolyDataBase()
    {
        type = POLYDATA_TYPE_NONE;
    }
};

// Struct for RGB point cloud
struct PointCloudRGB {
    PolyDataBase base;
    std::vector<glm::vec3> colors;

    PointCloudRGB()
    {
        base.type = POLYDATA_TYPE_POINTCLOUD_RGB;
    }
};

// Writes a polygonal dataset to a file in legacy VTK PolyData format
//
// Assumes that the file starts with a five line header section followed by a
// data section in ASCII format, i.e.:
//
// # vtk DataFile Version x.x\n
// Some information about the file\n
// ASCII\n
// DATASET POLYDATA\n
// POINTS 10 float\n
// raw data........\n
bool polydataSaveVTK(const PolyDataBase *polydata, const std::string &filename);

}  // namespace cg
