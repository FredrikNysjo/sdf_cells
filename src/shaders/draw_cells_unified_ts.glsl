#extension GL_ARB_shader_draw_parameters: require

#define CLUSTER_SIZE 64
#define CLUSTER_SIZE_HIDDEN 4
#define MAX_CLUSTERS_BRICK 4096
#define MAX_GRID_LEVEL 4
#define CUTOFF_TOLERENCE 0.1
#define ISOVALUE (128.0 / 255.0)
#define USE_OCCLUSION_CULLING 1
#define USE_FRUSTUM_CULLING 1
#define UPDATE_VISIBILITY_MASK_IN_TES 0
#define DISCARD_VERTEX { gl_Position = vec4(2.0, 2.0, 2.0, 0.0); return; }
#define DISCARD_TESSELATION { gl_TessLevelOuter[0] = gl_TessLevelOuter[1] = 0; return; }

#define SHAPE_SQUARE 0
#define SHAPE_ROUND 1
#define SHAPE_DISK 2
#define SHAPE_BOX 3
#define SHAPE_CELL 4

layout(binding = 0, std140) uniform SceneUniforms {
    mat4 view_from_world;
    mat4 proj_from_view;
    vec2 resolution;
    float time;
    int frame;
} u_scene;

layout(binding = 1, std430) restrict readonly buffer DrawableBuffer {
    mat4 world_from_local[];
} u_drawable;

layout(location = 0) uniform int u_point_shape;
layout(location = 1) uniform float u_point_scale;
layout(location = 2) uniform float u_lod_threshold_pixels;
layout(location = 3) uniform int u_freeze_visibility;
layout(binding = 0) uniform usamplerBuffer u_cells;
layout(binding = 1) uniform usamplerBuffer u_bricks;
layout(binding = 2) uniform usamplerBuffer u_lodinfo;
layout(binding = 3) uniform sampler2D u_depth;
layout(binding = 0, r32ui) restrict uniform uimageBuffer u_visibility;
layout(binding = 1, r32ui) restrict uniform uimageBuffer u_visibility2;

float cellLinear(vec3 p, vec4 c0, vec4 c1)
{
    return mix(mix(mix(c0[0], c0[1], p.x), mix(c0[2], c0[3], p.x), p.y),
               mix(mix(c1[0], c1[1], p.x), mix(c1[2], c1[3], p.x), p.y), p.z);
}

vec3 cellGrad(vec4 c0, vec4 c1)
{
    float grad_x = (c0[1] + c0[3] + c1[1] + c1[3]) - (c0[0] + c0[2] + c1[0] + c1[2]);
    float grad_y = (c0[2] + c0[3] + c1[2] + c1[3]) - (c0[0] + c0[1] + c1[0] + c1[1]);
    float grad_z = (c1[0] + c1[1] + c1[2] + c1[3]) - (c0[0] + c0[1] + c0[2] + c0[3]);
    return vec3(grad_x, grad_y, grad_z) * 0.25;
}

int findCutoffLOD(vec3 grid_pos, int grid_level, mat4 view_from_local, float view_radius)
{
    for (int i = MAX_GRID_LEVEL; i >= grid_level; --i) {
        // Snap input position to closest brick center in other grid level
        vec3 grid_pos_snapped = floor(grid_pos / (exp2(i - grid_level) * 16.0)) * 16.0 + 8.0;
        vec4 view_pos = view_from_local * vec4(grid_pos_snapped, 1.0);
        float point_size = (view_radius / length(view_pos.xyz)) * u_scene.proj_from_view[1][1] *
                           u_scene.resolution[1];
        float lod = clamp(log2(u_lod_threshold_pixels / point_size), 0, MAX_GRID_LEVEL);
        if (abs(lod - i) < 0.5 + CUTOFF_TOLERENCE) return int(lod);
    }
    return -1;
}

#if defined(VERTEX)

layout(location = 0) flat out int out_instanceID;

void main()
{
    // Pass-through
    out_instanceID = gl_InstanceID + gl_DrawIDARB;
}

#elif defined(TESS_CONTROL)

layout(vertices = 1) out;

layout(location = 0) flat in int in_instanceID[];

layout(location = 0) flat out int out_instanceID[];
layout(location = 1) flat out uvec4 out_brick[];

void main()
{
    int brick_id = gl_PrimitiveID;
    uvec4 brick = texelFetch(u_bricks, brick_id);
    int count = int(brick.y);  // Number of clusters to draw
    int grid_level = int(brick.z);
    vec3 grid_pos = vec3((brick.www >> uvec3(0u, 8u, 16u)) & 255u);

    mat4 view_from_local =
        u_scene.view_from_world * u_drawable.world_from_local[in_instanceID[gl_InvocationID]];
    float view_radius = 0.5 * length(view_from_local[0].xyz);
    int lod = findCutoffLOD(grid_pos, grid_level, view_from_local, view_radius);
    if (bool(u_freeze_visibility)) lod = grid_level;

#if USE_FRUSTUM_CULLING
    vec4 clip_pos = u_scene.proj_from_view * view_from_local * vec4(grid_pos, 1.0);
    vec3 ndc_pos = clip_pos.xyz / clip_pos.w;
    if (any(greaterThan(abs(ndc_pos), vec3(1.25)))) lod = -1;
#endif

    int visibility_id = brick_id + (in_instanceID[gl_InvocationID] * MAX_CLUSTERS_BRICK);
    int visibility_bit = visibility_id & 31;
    int visibility_ofs = visibility_id >> 5;
    uint visibility = imageLoad(u_visibility, visibility_ofs).r & (1u << visibility_bit);
#if !USE_OCCLUSION_CULLING
    visibility = 1u;
#endif

    out_brick[gl_InvocationID] = brick;
    out_instanceID[gl_InvocationID] = in_instanceID[gl_InvocationID];
#if defined(DRAW_CELLS_VISIBLE) || defined(DRAW_CELLS_EWA)
    gl_TessLevelOuter[0] = (grid_level == lod && visibility > 0u) ? clamp(count, 0, 64) : 0;
    gl_TessLevelOuter[1] = CLUSTER_SIZE - 1;
#elif defined(DRAW_CELLS_HIDDEN)
    gl_TessLevelOuter[0] = (grid_level == lod && visibility == 0u) ? clamp(count, 0, 64) : 0;
    gl_TessLevelOuter[1] = CLUSTER_SIZE_HIDDEN - 1;  // Draw hidden cells at reduced rate
#endif
}

#elif defined(TESS_EVALUATION)

layout(isolines, point_mode) in;

layout(location = 0) flat in int in_instanceID[];
layout(location = 1) flat in uvec4 in_brick[];

layout(location = 0) flat out ivec2 out_visibility;
layout(location = 1) flat out vec3 out_view_normal;
layout(location = 2) flat out uvec2 out_cell_scalars;
layout(location = 3) flat out float out_max_depth;
layout(location = 4) flat out mat3 out_tbn;

void main()
{
    int brick_id = gl_PrimitiveID;
    int first = int(in_brick[0].x);  // Offset to first cluster
    int grid_level = int(in_brick[0].z);
    float grid_scale = exp2(grid_level);

    int cluster_id = first + int(round(gl_TessCoord.y * gl_TessLevelOuter[0]));
    int cell_id = int(round(gl_TessCoord.x * gl_TessLevelOuter[1]));
#if defined(DRAW_CELLS_HIDDEN)
    // Apply offset to cell-ID so that the whole brick is sampled after N frames
    cell_id += (CLUSTER_SIZE_HIDDEN * u_scene.frame) % CLUSTER_SIZE;
#endif
    int visibility_id = brick_id + (in_instanceID[0] * MAX_CLUSTERS_BRICK);

    uvec4 cell = texelFetch(u_cells, cluster_id * CLUSTER_SIZE + cell_id);
    if (all(equal(uvec4(0u), cell))) DISCARD_VERTEX;
    vec4 c0 = unpackUnorm4x8(cell.z);
    vec4 c1 = unpackUnorm4x8(cell.w);
    vec3 local_pos = (vec3((cell.xxx >> uvec3(0u, 11u, 22u)) & 2047u) + 0.5) * grid_scale;
    vec3 local_normal = -normalize(cellGrad(c0, c1));
    float local_dist = (cellLinear(vec3(0.5), c0, c1) - ISOVALUE) * 24.0 * grid_scale;

#if defined(DRAW_CELLS_VISIBLE) || defined(DRAW_CELLS_EWA)
    if (u_point_shape == SHAPE_ROUND || u_point_shape == SHAPE_DISK) {
        // Project point onto the zero-distance isosurface
        local_pos.xyz += local_dist * local_normal;
    }
#elif defined(DRAW_CELLS_HIDDEN)
    // Project point onto the zero-distance isosurface
    local_pos.xyz += local_dist * local_normal;
#endif

    mat4 view_from_local = u_scene.view_from_world * u_drawable.world_from_local[in_instanceID[0]];
    vec4 view_pos = view_from_local * vec4(local_pos, 1.0);
    vec3 view_normal = normalize(mat3(view_from_local) * local_normal);
    float view_radius = 0.5 * length(view_from_local[0].xyz) * grid_scale;
    if (dot(view_pos.xyz, view_normal) > 0.0) DISCARD_VERTEX;

#if defined(DRAW_CELLS_VISIBLE) || defined(DRAW_CELLS_EWA)
    float point_scale = u_point_scale * length(view_pos.xyz / view_pos.z);
    gl_PointSize =
        (-view_radius / view_pos.z) * u_scene.proj_from_view[1][1] * u_scene.resolution[1];
    gl_PointSize *= point_scale;
    // Snap point size to nearest 1/8th pixel, to avoid sampling artifacts
    // caused by GL_POINT_SIZE_GRANULARITY limit
    gl_PointSize = ceil(gl_PointSize * 8.0) / 8.0;
#elif defined(DRAW_CELLS_HIDDEN)
    gl_PointSize = 1.0;
#endif

#if defined(DRAW_CELLS_EWA)
    // Compute and apply depth offset for EWA pass
    vec4 clip_pos = u_scene.proj_from_view *
                    vec4(view_pos.xyz + normalize(view_pos.xyz) * view_radius * 4.0, 1.0);
    view_pos.xyz += -normalize(view_pos.xyz) * view_radius * 4.0;
    out_max_depth = (clip_pos.z / clip_pos.w) * 0.5 + 0.5;
#endif
    gl_Position = u_scene.proj_from_view * view_pos;

#if defined(DRAW_CELLS_VISIBLE)
    // Compute vectors for interpolating ray origin and ray direction over billboard
    mat3 tbn = transpose(mat3(view_from_local) / length(view_from_local[0]));
    out_tbn = point_scale * tbn;
    out_tbn[2] = tbn * (view_pos.xyz / view_radius);
#endif

    out_visibility = ivec2(visibility_id + 1, cluster_id * CLUSTER_SIZE + cell_id);
    out_view_normal = view_normal;
    out_cell_scalars = cell.zw;

#if defined(DRAW_CELLS_HIDDEN) && UPDATE_VISIBILITY_MASK_IN_TES
    vec3 frag_pos = (gl_Position.xyz / gl_Position.w) * 0.5 + 0.5;
    if (textureLod(u_depth, frag_pos.xy, 0.0).r > frag_pos.z) {
        int visibility_bit = visibility_id & 31;
        int visibility_ofs = visibility_id >> 5;
        imageAtomicOr(u_visibility2, visibility_ofs, 1u << visibility_bit);
    }
    DISCARD_VERTEX;
#endif
}

#elif defined(FRAGMENT)

#if defined(DRAW_CELLS_HIDDEN)
layout(early_fragment_tests) in;
#endif

layout(location = 0) flat in ivec2 in_visibility;
layout(location = 1) flat in vec3 in_view_normal;
layout(location = 2) flat in uvec2 in_cell_scalars;
layout(location = 3) flat in float in_max_depth;
layout(location = 4) flat in mat3 in_tbn;

layout(location = 0) out vec4 out_gbuffer;
layout(location = 1) out vec2 out_visibility;

bool intersectUnitBox(vec3 ray_origin, vec3 ray_dir, out float tmin, out float tmax)
{
    // Branchless ray/AABB intersection test adapted from
    // https://tavianator.com/fast-branchless-raybounding-box-intersections/
    vec3 t1 = (-0.5 - 1e-3 - ray_origin) * clamp(1.0 / ray_dir, -9999.0, 9999.0);
    vec3 t2 = (0.5 + 1e-3 - ray_origin) * clamp(1.0 / ray_dir, -9999.0, 9999.0);
    tmin = max(min(t1[0], t2[0]), max(min(t1[1], t2[1]), min(t1[2], t2[2])));
    tmax = min(max(t1[0], t2[0]), min(max(t1[1], t2[1]), max(t1[2], t2[2])));
    return (tmax - tmin) > 0.0;
}

bool intersectCell(vec3 ray_origin, vec3 ray_dir, float tmin, float tmax)
{
    vec3 p0 = ray_origin + tmin * ray_dir + 0.5;
    vec3 p1 = ray_origin + tmax * ray_dir + 0.5;
    vec3 p2 = (p0 + p1) * 0.5;
    vec4 c0 = unpackUnorm4x8(in_cell_scalars[0]);
    vec4 c1 = unpackUnorm4x8(in_cell_scalars[1]);
    float s0 = cellLinear(p0, c0, c1);
    float s1 = cellLinear(p1, c0, c1);
    float s2 = cellLinear(p2, c0, c1);
    return s0 < ISOVALUE && max(s1, s2) >= ISOVALUE;
}

void main()
{
#if defined(DRAW_CELLS_VISIBLE)
    bool hit = false;
    if (u_point_shape == SHAPE_SQUARE) {
        hit = true;
    } else if (u_point_shape == SHAPE_ROUND) {
        hit = length(gl_PointCoord - 0.5) < 0.5;
    } else if (u_point_shape == SHAPE_DISK) {
        vec2 xy = (gl_PointCoord * 2.0 - 1.0) * vec2(1.0, -1.0);
        float dz = dot(-in_view_normal.xy / (abs(in_view_normal.z) + 0.2), xy);
        hit = length(vec3(xy, dz)) <= 1.0;
    } else if (u_point_shape == SHAPE_BOX || u_point_shape == SHAPE_CELL) {
        vec2 xy = gl_PointCoord - 0.5;
        vec3 ray_origin = xy.x * in_tbn[0] - xy.y * in_tbn[1];
        vec3 ray_dir = in_tbn[2] + 2.0 * ray_origin;

        float tmin, tmax;
        hit = intersectUnitBox(ray_origin, ray_dir, tmin, tmax);
        if (u_point_shape == SHAPE_CELL)
            hit = hit && intersectCell(ray_origin, ray_dir, tmin, tmax);
    }
    if (!hit) discard;
#endif

#if defined(DRAW_CELLS_VISIBLE)
    out_gbuffer = vec4(in_view_normal, 1.0) * (u_point_shape == SHAPE_BOX ? 10.0 : 0.1);
    out_visibility = intBitsToFloat(in_visibility);
#elif defined(DRAW_CELLS_HIDDEN)
    int visibility_id = in_visibility[0] - 1;
    int visibility_bit = visibility_id & 31;
    int visibility_ofs = visibility_id >> 5;
    imageAtomicOr(u_visibility2, visibility_ofs, 1u << visibility_bit);
#elif defined(DRAW_CELLS_EWA)
    float fade = float(texelFetch(u_depth, ivec2(gl_FragCoord.xy), 0).r < in_max_depth);
    fade *= clamp(1.0 - length(gl_PointCoord - 0.5) * 2.0, 0.0, 1.0);
    out_gbuffer = vec4(in_view_normal, 1.0) * fade;
    out_visibility = intBitsToFloat(in_visibility);
#endif
}

#endif
