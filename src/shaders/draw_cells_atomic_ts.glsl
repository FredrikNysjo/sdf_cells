#extension GL_ARB_shader_draw_parameters: require

#define CLUSTER_SIZE 64
#define CLUSTER_SIZE_HIDDEN 4
#define MAX_CLUSTERS_BRICK 4096
#define MAX_BRICK_LEVEL 4
#define CUTOFF_TOLERENCE 0.6
#define ISOVALUE (128.0 / 255.0)
#define USE_OCCLUSION_CULLING 1
#define USE_FRUSTUM_CULLING 1
#define DISCARD_VERTEX { gl_Position = vec4(2.0, 2.0, 2.0, 0.0); return; }
#define DISCARD_TESSELATION { gl_TessLevelOuter[0] = gl_TessLevelOuter[1] = 0; return; }

#define SHAPE_SQUARE 0
#define SHAPE_ROUND 1
#define SHAPE_DISK 2
#define SHAPE_BOX 3
#define SHAPE_CELL 4

layout(binding = 0, std140) uniform SceneUniforms {
    mat4 view_from_world;
    mat4 proj_from_view;
    vec2 resolution;
    float time;
    int frame;
} u_scene;

layout(binding = 1, std430) restrict readonly buffer DrawableBuffer {
    mat4 world_from_local[];
} u_drawable;

layout(location = 0) uniform int u_point_shape;
layout(location = 1) uniform float u_point_scale;
layout(location = 2) uniform float u_lod_threshold_pixels;
layout(location = 3) uniform int u_freeze_visibility;
layout(location = 4) uniform int u_op;
layout(binding = 0) uniform usamplerBuffer u_cells;
layout(binding = 1) uniform usamplerBuffer u_bricks;
layout(binding = 2) uniform usamplerBuffer u_lodinfo;
layout(binding = 0, r32ui) restrict uniform uimageBuffer u_visibility;
layout(binding = 1, r32ui) restrict uniform uimage2D u_depth2;
layout(binding = 3, rgba16f) restrict uniform image2D u_gbuffer;
layout(binding = 4, rg32ui) restrict uniform uimage2D u_id;

vec2 lds_r2(float n)
{
    float phi = 1.3247179572;
    return fract(n / vec2(phi, phi * phi));
}

float cellLinear(vec3 p, vec4 c0, vec4 c1)
{
    return mix(mix(mix(c0[0], c0[1], p.x), mix(c0[2], c0[3], p.x), p.y),
               mix(mix(c1[0], c1[1], p.x), mix(c1[2], c1[3], p.x), p.y), p.z);
}

vec3 cellGrad(vec4 c0, vec4 c1)
{
    float grad_x = (c0[1] + c0[3] + c1[1] + c1[3]) - (c0[0] + c0[2] + c1[0] + c1[2]);
    float grad_y = (c0[2] + c0[3] + c1[2] + c1[3]) - (c0[0] + c0[1] + c1[0] + c1[1]);
    float grad_z = (c1[0] + c1[1] + c1[2] + c1[3]) - (c0[0] + c0[1] + c0[2] + c0[3]);
    return vec3(grad_x, grad_y, grad_z) * 0.25;
}

int findCutoffLOD(vec3 local_pos, int level, mat4 view_from_local, float view_radius)
{
    for (int i = MAX_BRICK_LEVEL; i >= level; --i) {
        // Snap input position to closest brick center in other grid level
        vec3 local_pos_snapped = floor(local_pos / (exp2(i - level) * 16.0)) * 16.0 + 8.0;
        vec4 view_pos = view_from_local * vec4(local_pos_snapped, 1.0);
        float point_size = (view_radius / length(view_pos.xyz)) * u_scene.proj_from_view[1][1] *
                           u_scene.resolution[1];
        float cutoff = clamp(log2(u_lod_threshold_pixels / point_size), 0, MAX_BRICK_LEVEL);
        if (abs(cutoff - i) < CUTOFF_TOLERENCE) return int(cutoff);
    }
    return -1;
}

int findLOD(vec3 grid_pos, mat4 view_from_local, float view_radius)
{
    vec4 view_pos = view_from_local * vec4(grid_pos, 1.0);
    float point_size = (view_radius / length(view_pos.xyz)) * u_scene.proj_from_view[1][1] *
                       u_scene.resolution[1];
    float lod = clamp(log2(u_lod_threshold_pixels / point_size), 0, MAX_BRICK_LEVEL);
    return int(lod);
}

vec2 snapToEvenPixel(vec2 ndc_pos, vec2 resolution)
{
    vec2 frag_pos = (ndc_pos * 0.5 + 0.5) * resolution;
    vec2 frag_pos_snapped = floor(frag_pos * 0.5) * 2.0;
    vec2 ndc_pos_snapped = (frag_pos_snapped / resolution) * 2.0 - 1.0;
    return ndc_pos_snapped;
}

#if defined(VERTEX)

void main()
{
    // Pass-through
}

#elif defined(TESS_CONTROL)

layout(vertices = 1) out;

layout(location = 0) flat out uvec4 out_lodinfo[];

void main()
{
    mat4 view_from_local = u_scene.view_from_world * u_drawable.world_from_local[gl_PrimitiveID];
    float view_radius = 0.5 * length(view_from_local[0].xyz);

    uvec4 lodinfo_base = texelFetch(u_lodinfo, 0);
    int min_lod = MAX_BRICK_LEVEL;
    for (int i = 0; i < 8; ++i) {
        vec3 grid_dim = vec3(lodinfo_base.xyz);
        vec3 grid_pos_i = vec3((uvec3(i) >> uvec3(0u, 1u, 2u)) & 1u) * grid_dim;
        min_lod = min(findLOD(grid_pos_i, view_from_local, view_radius), min_lod);
    }

    uvec4 lodinfo = texelFetch(u_lodinfo, min_lod);
    int count = int(lodinfo.w);

    out_lodinfo[gl_InvocationID] = lodinfo;
    gl_TessLevelOuter[0] = (count / 64) + min(1, count % 64);
    gl_TessLevelOuter[1] = max(1, min(64, count) - 1);
}

#elif defined(TESS_EVALUATION)

layout(isolines, point_mode) in;

layout(location = 0) flat in uvec4 in_lodinfo[];

layout(location = 0) flat out uvec4 out_metadata;
layout(location = 1) flat out uvec4 out_brick;

void main()
{
    int instance_id = gl_PrimitiveID;
    int brick_id = int(round(gl_TessCoord.y * gl_TessLevelOuter[0]) * 64 +
                       round(gl_TessCoord.x * gl_TessLevelOuter[1]));
    if (brick_id >= in_lodinfo[0].w) DISCARD_VERTEX;

    int visibility_id = brick_id + (instance_id * MAX_CLUSTERS_BRICK);
    int visibility_bit = visibility_id & 31;
    int visibility_ofs = visibility_id >> 5;
    uint visibility = imageLoad(u_visibility, visibility_ofs).r & (1u << visibility_bit);
#if !USE_OCCLUSION_CULLING
    visibility = 1u;
#endif
    if (bool(u_freeze_visibility) && visibility == 0u) DISCARD_VERTEX;

    uvec4 brick = texelFetch(u_bricks, brick_id);
    int first = int(brick.x);  // Offset to first cluster
    int count = int(brick.y);  // Number of clusters to draw
    int level = int(brick.z);  // LOD
    vec3 local_pos = vec3((brick.www >> uvec3(0u, 8u, 16u)) & 255u);

    mat4 view_from_local = u_scene.view_from_world * u_drawable.world_from_local[instance_id];
    float view_radius = 0.5 * length(view_from_local[0].xyz);

    int cutoff = findCutoffLOD(local_pos, level, view_from_local, view_radius);
    if (!bool(u_freeze_visibility) && level != cutoff) DISCARD_VERTEX;

#if USE_FRUSTUM_CULLING
    vec4 clip_pos = u_scene.proj_from_view * view_from_local * vec4(local_pos, 1.0);
    vec3 ndc_pos = clip_pos.xyz / clip_pos.w;
    if (any(greaterThan(abs(ndc_pos), vec3(1.25)))) DISCARD_VERTEX;
#endif

    gl_PointSize = visibility > 0u ? 8.0 : sqrt(CLUSTER_SIZE_HIDDEN);  // Select draw rate
    gl_Position = vec4((lds_r2(visibility_ofs) - 0.5), 0.0, 1.0);  // Jitter to avoid "overdraw"
    // Snap point center to even pixel to improve thread utilization and avoid artifacts
    gl_Position.xy = snapToEvenPixel(gl_Position.xy, u_scene.resolution);
    out_metadata = uvec4(instance_id, brick_id, visibility, visibility_id);
    out_brick = brick;
}

#elif defined(FRAGMENT)

layout(early_fragment_tests) in;

layout(location = 0) flat in uvec4 in_metadata;
layout(location = 1) flat in uvec4 in_brick;

void main()
{
    uint instance_id = in_metadata.x;
    uint brick_id = in_metadata.y;
    uint visibility = in_metadata.z;
    uint visibility_id = in_metadata.w;

    int cell_id = int(gl_PointCoord.y * 8) * 8 + int(gl_PointCoord.x * 8);
    if (visibility == 0u) cell_id = (cell_id + u_scene.frame) % CLUSTER_SIZE;

    int first = int(in_brick.x);  // Offset to first cluster
    int count = int(in_brick.y);  // Number of clusters to draw
    int level = int(in_brick.z);  // LOD

    float level_scale = exp2(level);
    mat4 view_from_local = u_scene.view_from_world * u_drawable.world_from_local[instance_id];
    float view_radius = 0.5 * length(view_from_local[0].xyz) * level_scale;

    for (int n = 0; n < count; ++n) {
        uvec4 cell = texelFetch(u_cells, (first + n) * CLUSTER_SIZE + cell_id);
        if (all(equal(uvec4(0u), cell))) continue;
        vec4 c0 = unpackUnorm4x8(cell.z);
        vec4 c1 = unpackUnorm4x8(cell.w);
        vec3 local_pos = (vec3((cell.xxx >> uvec3(0u, 11u, 22u)) & 2047u) + 0.5) * level_scale;
        vec3 local_normal = -normalize(cellGrad(c0, c1));

        // Project point onto the zero-distance isosurface
        float local_dist = (cellLinear(vec3(0.5), c0, c1) - ISOVALUE) * 24.0 * level_scale;
        local_pos.xyz += local_dist * local_normal;

        vec4 view_pos = view_from_local * vec4(local_pos, 1.0);
        vec3 view_normal = normalize(mat3(view_from_local) * local_normal);
        if (dot(view_pos.xyz, view_normal) > 0.0) continue;

        // Do atomic splatting

        float point_size =
            (-view_radius / view_pos.z) * u_scene.proj_from_view[1][1] * u_scene.resolution[1];
        point_size *= u_point_scale * length(view_pos.xyz / view_pos.z);
        // Snap point size to nearest 1/8th pixel (for consistency with non-atomics version)
        point_size = ceil(point_size * 8.0) / 8.0;
        if (visibility == 0u) point_size = min(1.0, point_size);

        vec4 clip_pos = u_scene.proj_from_view * view_pos;
        vec3 ndc_pos = clip_pos.xyz / clip_pos.w;
        if (any(greaterThanEqual(abs(ndc_pos), vec3(1.0)))) continue;

        vec2 frag_pos = u_scene.resolution * (ndc_pos.xy * 0.5 + 0.5);
        float frag_depth = ndc_pos.z * 0.5 + 0.5;

        ivec2 aabb_min = ivec2(round(frag_pos - point_size * 0.5));
        ivec2 aabb_max = ivec2(round(frag_pos + point_size * 0.5));
        for (int y = aabb_min.y; y < aabb_max.y; ++y) {
            for (int x = aabb_min.x; x < aabb_max.x; ++x) {
                vec2 frag_pos_i = vec2(x, y) + 0.5;
                vec2 uv = (frag_pos_i - frag_pos) / point_size;
                if (length(uv) > 0.5) continue;

                uint prev_depth = imageLoad(u_depth2, ivec2(frag_pos_i)).r;
                if (u_op == 0 && prev_depth > floatBitsToUint(frag_depth)) {
                    imageAtomicMin(u_depth2, ivec2(frag_pos_i), floatBitsToUint(frag_depth));
                }
                if (u_op == 1 && prev_depth == floatBitsToUint(frag_depth)) {
                    imageStore(u_id, ivec2(frag_pos_i), uvec4(visibility_id + 1, cell_id, 0u, 0u));
                    imageStore(u_gbuffer, ivec2(frag_pos_i), vec4(view_normal, 1.0));
                }
            }
        }
    }
}

#endif
