layout(binding = 0) uniform sampler2D u_id;
layout(binding = 0, r32ui) restrict uniform uimageBuffer u_visibility;

#if defined(COMPUTE)

layout(local_size_x = 8, local_size_y = 8) in;

void main()
{
    vec2 frag_pos = vec2(gl_WorkGroupID.xy * gl_WorkGroupSize.xy + gl_LocalInvocationID.xy) + 0.5;

    vec2 texel = texelFetch(u_id, ivec2(frag_pos), 0).rg;
    int visibility_id = floatBitsToInt(texel.r) - 1;
    if (visibility_id < 0) return;

    // Note: for some reason, using integer modulo/division to compute the
    // following values gives visibility artifacts on AMD Vega, when
    // MAX_NUM_INSTANCES exceeds 2048. But we should use bitwise operators
    // instead anyways, since they are much faster.
    int visibility_bit = visibility_id & 31;
    int visibility_ofs = visibility_id >> 5;
    uint mask = 1u << visibility_bit;

    uint prev = imageLoad(u_visibility, visibility_ofs).r;
    if ((prev & mask) == 0u)
        imageAtomicOr(u_visibility, visibility_ofs, mask);
}

#endif
