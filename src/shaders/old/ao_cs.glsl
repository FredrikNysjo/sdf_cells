#define NUM_SAMPLES 4
#define MAX_TEMPORAL_FRAMES 64
#define AO_RADIUS_WORLD 0.05
#define AO_SCALE_FACTOR 1.2
#define MAX_SAMPLE_RADIUS 32.0
#define TWO_PI 6.283184

layout(binding = 0) uniform sampler2D u_depth;
layout(binding = 1, r8) restrict writeonly uniform image2D u_ao;

layout(binding = 0, std140) uniform SceneUniforms {
    mat4 view;
    mat4 projection;
    vec2 resolution;
    float time;
    int frame;
} u_scene;

#if defined(COMPUTE)

layout(local_size_x = 8, local_size_y = 8) in;

vec2 vogel_disk(int i, int n, float r_offset, float phi_offset)
{
    float golden_angle = 2.39996;
    float phi = float(i) * golden_angle + phi_offset * TWO_PI;
    float r = fract((float(i) + r_offset) / float(n));
    return sqrt(r) * vec2(sin(phi), cos(phi));
}

float reconstructViewPos(float depth, mat4 projection)
{
    float c = projection[2][2];
    float d = projection[3][2];
    float ndc_pos_z = depth * 2.0 - 1.0;
    return -d / (ndc_pos_z + c);
}

float pointSize(float view_pos_z, float view_radius, mat4 projection, vec2 resolution)
{
    return (-view_radius / view_pos_z) * projection[1][1] * resolution[1];
}

void main()
{
    vec2 frag_pos = vec2(gl_WorkGroupID.xy * gl_WorkGroupSize.xy + gl_LocalInvocationID.xy) + 0.5;
    int frame = u_scene.frame % MAX_TEMPORAL_FRAMES;

    float depth = texelFetch(u_depth, ivec2(frag_pos), 0).r;
    float view_pos_z = reconstructViewPos(depth, u_scene.projection);
    float view_radius = AO_RADIUS_WORLD;
    float point_size = pointSize(view_pos_z, view_radius, u_scene.projection, u_scene.resolution);
    float ao_radius = min(MAX_SAMPLE_RADIUS, point_size * 0.5);

    float visibility = 0.0;
    if (depth < 1.0) {
        float seed = fract(dot(vec2(0.754877, 0.569840), frag_pos));
        float r_offset = fract(sin(frame + seed) * 43758.5453);
        float phi_offset = fract(frame * sqrt(3.0) + seed);

        visibility += 0.5;  // Include contribution from center sample
        for (int i = 0; i < NUM_SAMPLES; ++i) {
            vec2 disk = vogel_disk(i, NUM_SAMPLES, r_offset, phi_offset);
            float texel = texelFetch(u_depth, ivec2(frag_pos + disk * ao_radius), 0).r;
            float z_diff = reconstructViewPos(texel, u_scene.projection) - view_pos_z;
            float falloff = 0.25 * smoothstep(view_radius, 4.0 * view_radius, z_diff);
            visibility += max(falloff, min(1.0, (-z_diff / view_radius) * 0.5 + 0.5));
        }
    }
    visibility /= (NUM_SAMPLES + 1);
    visibility = clamp(visibility * AO_SCALE_FACTOR, 0.0, 1.0);

    imageStore(u_ao, ivec2(frag_pos), vec4(visibility));
}

#endif
