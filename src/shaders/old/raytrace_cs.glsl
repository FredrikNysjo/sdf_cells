#define META_SIZE 8
#define CLUSTER_SIZE 16
#define BLOCKS_PER_SIDE 16
#define EPSILON 1e-5
#define HALF_EPSILON (0.5 * EPSILON)
#define MAX_BLOCK_STEPS 36
#define MAX_BOUNCES 3
#define BASE_ATTENUATION 0.8
#define HDR_CLAMPING 3.0
#define M_PI 3.141592

#define MATERIAL_MATTE 0
#define MATERIAL_METAL 1

layout(binding = 0, std140) uniform SceneUniforms {
    mat4 view;
    mat4 proj;
    vec2 res;
    float time;
    int frame;
} u_scene;

layout(binding = 1, std140) uniform DrawableUniforms {
    mat4 model;
    mat4 normal;
} u_drawable;

layout(location = 0) uniform ivec3 u_volume_dim;
layout(location = 1) uniform int u_material = 0;
layout(location = 2) uniform int u_show_envmap = 1;
layout(binding = 0) uniform samplerBuffer u_cells;
layout(binding = 1) uniform usamplerBuffer u_metacells;
layout(binding = 2) uniform usamplerBuffer u_blocks;
layout(binding = 3) uniform sampler2D u_bluenoise;
layout(binding = 4) uniform sampler2D u_envmap;
layout(binding = 5) uniform sampler2D u_depth;
layout(binding = 1, rgba16f) uniform restrict readonly image2D u_gbuffer;
layout(binding = 2, rgba16f) uniform restrict writeonly image2D u_color;

vec3 reconstructViewPos(vec3 ndc_pos, mat4 proj)
{
    float a = proj[0][0];
    float b = proj[1][1];
    float c = proj[2][2];
    float d = proj[3][2];
    float e = proj[2][0];
    float f = proj[2][1];
    float view_pos_z = -d / (ndc_pos.z + c);
    float view_pos_x = (-view_pos_z / a) * (ndc_pos.x + e);
    float view_pos_y = (-view_pos_z / b) * (ndc_pos.y + f);
    return vec3(view_pos_x, view_pos_y, view_pos_z);
}

vec3 sampleEnvMap(vec3 world_dir)
{
    vec3 dir = normalize(world_dir).xzy;
    float phi = atan(dir.y, dir.x);
    float theta = acos(dir.z);
    return texture(u_envmap, vec2((phi / 3.141592) * 0.5 + 0.5, theta / 3.141592)).rgb;
}

vec3 cellGrad(vec4 c0, vec4 c1)
{
    float grad_x = (c0[1] + c0[3] + c1[1] + c1[3]) - (c0[0] + c0[2] + c1[0] + c1[2]);
    float grad_y = (c0[2] + c0[3] + c1[2] + c1[3]) - (c0[0] + c0[1] + c1[0] + c1[1]);
    float grad_z = (c1[0] + c1[1] + c1[2] + c1[3]) - (c0[0] + c0[1] + c0[2] + c0[3]);
    return vec3(grad_x, grad_y, grad_z) * 0.25;
}

// https://tavianator.com/fast-branchless-raybounding-box-intersections/
bool intersectBox(vec3 aabb[2], vec3 ray_origin, vec3 ray_dir_inv, out float tmin, out float tmax)
{
    vec3 t1 = (aabb[0] - ray_origin) * ray_dir_inv;
    vec3 t2 = (aabb[1] - ray_origin) * ray_dir_inv;
    tmin = max(0.0, max(min(t1[0], t2[0]), max(min(t1[1], t2[1]), min(t1[2], t2[2]))));
    tmax = max(0.0, min(max(t1[0], t2[0]), min(max(t1[1], t2[1]), max(t1[2], t2[2]))));

    return bool((tmax - tmin) > 0.0);
}

bool intersectBlock(uvec4 block, vec3 ray_origin, vec3 ray_dir, out float t, int steps, out vec3 normal)
{
    int count = int(block.x);
    int first = int(block.y);
    if (count == 0) return false;

    float max_dim = max(u_volume_dim.x, max(u_volume_dim.y, u_volume_dim.z));
    vec3 half_dim = 0.5 * u_volume_dim;

    vec3 aabb_cluster[2], aabb_metacell[2], aabb_cell[2];
    vec3 ray_dir_inv = clamp(1.0 / ray_dir, -9999.0, 9999.0);
    vec4 closest = vec4(ray_origin * (0.5 * max_dim) + 0.5 * half_dim, 9999.0);
    uvec3 hit_info = uvec3(0);
    float tmin, tmax;

    for (int k = 0; k < count; ++k) {
        // Find next candidate cluster along the ray
        for (; k < count; ++k) {
            int offset = (first + k) * CLUSTER_SIZE;
            uvec3 cluster = texelFetch(u_metacells, offset + (CLUSTER_SIZE - 1)).rgb;
            aabb_cluster[0] = vec3((cluster.xxy >> uvec3(0u, 16u, 0u)) & 65535u);
            aabb_cluster[1] = aabb_cluster[0] + vec3((cluster.yzz >> uvec3(16u, 0u, 16u)) & 65535u) + 1.0;

            if (intersectBox(aabb_cluster, closest.xyz, ray_dir_inv, tmin, tmax) && tmin < closest.w)
                break;
        }

        int offset = (first + k) * CLUSTER_SIZE;
        for (int j = 0; j < (CLUSTER_SIZE - 1) && k < count; ++j) {
            uvec3 metacell = texelFetch(u_metacells, offset + j).rgb;
            aabb_metacell[0] = vec3((metacell.xxy >> uvec3(0u, 16u, 0u)) & 65535u);
            aabb_metacell[1] = aabb_metacell[0] + 1.0;
            uint mask = (metacell.y >> 16u) & 255u;

            if (intersectBox(aabb_metacell, closest.xyz, ray_dir_inv, tmin, tmax) && tmin < closest.w) {
                for (int i = 0; i < META_SIZE; ++i) {
                    aabb_cell[0] = aabb_metacell[0] + vec3(i & 1, (i >> 1) & 1, i >> 2) * 0.5;
                    aabb_cell[1] = aabb_cell[0] + 0.5;

                    if (intersectBox(aabb_cell, closest.xyz, ray_dir_inv, tmin, tmax) && tmin < closest.w) {
                        if ((mask & (1u << i)) > 0u) {
                            hit_info = uvec3(mask, i, metacell.z);
                            closest.w = tmin;
                        }
                    }
                }
            }
        }
    }

    if (closest.w < 9999.0) {
        uint mask = hit_info.x;
        uint cell_offset = block.z + hit_info.z + bitCount(mask & ~(0xffu << hit_info.y));

        vec4 c0 = texelFetch(u_cells, int(cell_offset * 2 + 0));
        vec4 c1 = texelFetch(u_cells, int(cell_offset * 2 + 1));
        normal = -normalize(cellGrad(c0, c1));
    }

    t = closest.w / (0.5 * max_dim);
    return closest.w < 9999.0;
}

bool intersectBlockVolume(vec3 ray_origin, vec3 ray_dir, out float tmin, out float tmax, out vec3 normal)
{
    const int NUM_BLOCKS = BLOCKS_PER_SIDE * BLOCKS_PER_SIDE * BLOCKS_PER_SIDE;

    vec3 aspect = vec3(u_volume_dim) / max(u_volume_dim.x, max(u_volume_dim.y, u_volume_dim.z));
    vec3 ray_dir_inv = clamp(1.0 / ray_dir, -9999.0, 9999.0);
    vec3 aabb_volume[2] = vec3[](-0.5 * aspect, 0.5 * aspect);
    bool hit = intersectBox(aabb_volume, ray_origin, ray_dir_inv, tmin, tmax);
    bool inside = true;

    if (hit) {
        vec3 p = clamp((ray_origin + (tmin + EPSILON) * ray_dir) / aspect + 0.5, 0.0, 0.9999);
        vec3 q = fract(p * BLOCKS_PER_SIDE) - 0.5;
        vec3 aabb_block[2] = vec3[](vec3(-0.5 - HALF_EPSILON), vec3(0.5 + HALF_EPSILON));
        uvec4 block = uvec4(0u);

        hit = false;
        for (int k = 0; k < MAX_BLOCK_STEPS && inside; ++k) {
            // Advance to next non-empty block
            for (; k < MAX_BLOCK_STEPS && inside; ++k) {
                int block_id = int(dot(floor(p * BLOCKS_PER_SIDE), vec3(1, 16, 256)));
                block = texelFetch(u_blocks, max(0, min(NUM_BLOCKS, block_id)));
                if (block.r > 0u) break;

                intersectBox(aabb_block, q, ray_dir_inv * aspect, tmin, tmax);
                p += (tmax + EPSILON) * (1.0 / BLOCKS_PER_SIDE) * ray_dir / aspect;
                q = fract(p * BLOCKS_PER_SIDE) - 0.5;
                inside = all(lessThan(abs(p - 0.5), vec3(0.5)));
            }

            if (hit = intersectBlock(block, ray_origin, ray_dir, tmin, k, normal))
                break;

            intersectBox(aabb_block, q, ray_dir_inv * aspect, tmin, tmax);
            p += (tmax + EPSILON) * (1.0 / BLOCKS_PER_SIDE) * ray_dir / aspect;
            q = fract(p * BLOCKS_PER_SIDE) - 0.5;
            inside = all(lessThan(abs(p - 0.5), vec3(0.5)));
        }
    }

    return hit;
}

#if defined(COMPUTE)

layout(local_size_x = 8, local_size_y = 8) in;

void main()
{
    vec2 frag_pos = vec2(gl_WorkGroupID.xy * gl_WorkGroupSize.xy + gl_LocalInvocationID.xy) + 0.5;
    int frame = u_scene.frame % 1024;

    float frag_depth = texelFetch(u_depth, ivec2(frag_pos), 0).r;
    vec3 view_normal = normalize(imageLoad(u_gbuffer, ivec2(frag_pos)).rgb);
    float point_size = imageLoad(u_gbuffer, ivec2(frag_pos)).a;
    if (frag_depth == 1.0) {
        if (bool(u_show_envmap)) {
            vec3 ndc_pos = vec3(frag_pos / u_scene.res, 0.0) * 2.0 - 1.0;
            vec3 view_pos = reconstructViewPos(ndc_pos, u_scene.proj);
            vec3 output_color = clamp(sampleEnvMap(view_pos), 0.0, HDR_CLAMPING);
            imageStore(u_color, ivec2(frag_pos), vec4(output_color, point_size));
        }
        return;
    }

    vec3 ndc_pos = vec3(frag_pos / u_scene.res, frag_depth) * 2.0 - 1.0;
    vec3 view_pos = reconstructViewPos(ndc_pos, u_scene.proj);

    mat4 local_from_view = inverse(u_scene.view);
    vec3 local_pos = vec3(local_from_view * vec4(view_pos, 1.0));
    vec3 local_normal = mat3(local_from_view) * view_normal;
    vec3 L = normalize(vec3(0.5, 1.0, 2.0) - view_pos);

    vec3 hit_record[MAX_BOUNCES];
    int count = 0;

    vec3 ray_dir = normalize(mat3(local_from_view) * view_pos);
    vec3 ray_origin = local_pos;

    for (int i = 0; i < MAX_BOUNCES; ++i) {
        vec3 seed = texelFetch(u_bluenoise, ivec2(frag_pos) % 128, 0).rgb;
        vec3 rnd = fract(seed + vec3(sqrt(2.0), sqrt(3.0), sqrt(5.0)) * (MAX_BOUNCES * frame + i));
        float u = rnd.x * 2.0 - 1.0;
        float phi = rnd.y * 2.0 * M_PI;
        vec3 rnd_ball = sqrt(rnd.z) * vec3(vec2(cos(phi), sin(phi)) * sqrt(1.0 - u * u), u);

        if (u_material == MATERIAL_MATTE)
            ray_dir = normalize(local_normal + rnd_ball);
        if (u_material == MATERIAL_METAL)
            ray_dir = normalize(reflect(ray_dir, local_normal) + rnd * 0.2);
        ray_origin = local_pos + 5e-3 * (local_normal + ray_dir);

        float tmin, tmax;
        hit_record[count] = mat3(u_scene.view) * ray_dir;
        if (!intersectBlockVolume(ray_origin, ray_dir, tmin, tmax, local_normal)) break;

        local_pos = ray_origin + tmin * ray_dir;
        count += 1;
    }

    vec3 output_color = vec3(0.0);
    {
        vec3 attenuation;
        if (u_material == MATERIAL_MATTE)
            attenuation = vec3(1.0, 0.92, 0.8) * BASE_ATTENUATION;
        if (u_material == MATERIAL_METAL)
            attenuation = vec3(1.0, 0.7, 0.3) * BASE_ATTENUATION;

        if (count < MAX_BOUNCES)
            output_color = sampleEnvMap(hit_record[count]) * attenuation;
        for (int i = count - 1; i >= 0; --i) {
            output_color = output_color * attenuation;
        }
        output_color = clamp(output_color, 0.0, HDR_CLAMPING);
    }

    imageStore(u_color, ivec2(frag_pos), vec4(output_color, point_size));
}

#endif
