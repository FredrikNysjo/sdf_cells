#define META_SIZE 8
#define CLUSTER_SIZE 16
#define MAX_CLUSTERS (1 << 20)
#define USE_OPTIMIZED_VERSION 1

layout(binding = 0, std430) restrict buffer DrawBuffer {
    ivec4 cmds[4];
    ivec2 data[];
} u_draw_buffer;

layout(binding = 1) uniform usamplerBuffer u_visibility;
layout(binding = 2) uniform usamplerBuffer u_bricks;

#if defined(VERTEX)

void main()
{
    uvec4 texel = texelFetch(u_bricks, gl_VertexID);
    int count = int(texel.x);
    int first = int(texel.y);
    int offset = int(texel.z);

#if !USE_OPTIMIZED_VERSION
    for (int k = 0; k < count; ++k) {
        int cluster_id = first + k;
        int cluster_bit = cluster_id & 31;
        uint visibility = texelFetch(u_visibility, cluster_id >> 5).r & (1u << cluster_bit);

        int level = (visibility > 0u ? 1 : 0);
        int stride = (level > 0 ? META_SIZE : 1) * CLUSTER_SIZE;
        int count = atomicAdd(u_draw_buffer.cmds[level].x, stride);
        u_draw_buffer.data[level * MAX_CLUSTERS + (count / stride)] = ivec2(cluster_id, offset);
    }
#else // USE_OPTIMIZED_VERSION
    ivec2 n = ivec2(0);
    for (int k = 0; k < count; ++k) {
        int cluster_id = first + k;
        int cluster_bit = cluster_id & 31;
        uint visibility = texelFetch(u_visibility, cluster_id >> 5).r & (1u << cluster_bit);

        int level = (visibility > 0u ? 1 : 0);
        n[level] += 1;
    }

    ivec2 stride = ivec2(1, META_SIZE) * CLUSTER_SIZE;
    n[0] = atomicAdd(u_draw_buffer.cmds[0].x, n[0] * stride[0]) / stride[0];
    n[1] = atomicAdd(u_draw_buffer.cmds[1].x, n[1] * stride[1]) / stride[1];

    for (int k = 0; k < count; ++k) {
        int cluster_id = first + k;
        int cluster_bit = cluster_id & 31;
        uint visibility = texelFetch(u_visibility, cluster_id >> 5).r & (1u << cluster_bit);

        int level = (visibility > 0u ? 1 : 0);
        u_draw_buffer.data[level * MAX_CLUSTERS + n[level]] = ivec2(cluster_id, offset);
        n[level] += 1;
    }
#endif // USE_OPTIMIZED_VERSION
}

#endif
