#define CLUSTER_SIZE 64
#define MAX_CLUSTERS_BRICK 4096
#define ISOVALUE (128.0 / 255.0)
#define DISCARD_VERTEX { gl_Position = vec4(2.0, 2.0, 2.0, 0.0); return; }

#define SHAPE_SQUARE 0
#define SHAPE_ROUND 1
#define SHAPE_DISK 2
#define SHAPE_BOX 3
#define SHAPE_CELL 4

layout(binding = 0, std140) uniform SceneUniforms {
    mat4 view;
    mat4 proj;
    vec2 res;
} u_scene;

layout(binding = 1, std430) restrict readonly buffer DrawableBuffer {
    mat4 model[];
} u_drawable;

layout(location = 0) uniform int u_point_shape;
layout(location = 1) uniform float u_point_scale;
layout(location = 2) uniform float u_lod_threshold_pixels;
layout(binding = 0) uniform usamplerBuffer u_cells;
layout(binding = 1) uniform usamplerBuffer u_bricks;
layout(binding = 2) uniform usamplerBuffer u_visibility;

float cellLinear(vec3 p, vec4 c0, vec4 c1)
{
    return mix(mix(mix(c0[0], c0[1], p.x), mix(c0[2], c0[3], p.x), p.y),
               mix(mix(c1[0], c1[1], p.x), mix(c1[2], c1[3], p.x), p.y), p.z);
}

vec3 cellGrad(vec4 c0, vec4 c1)
{
    float grad_x = (c0[1] + c0[3] + c1[1] + c1[3]) - (c0[0] + c0[2] + c1[0] + c1[2]);
    float grad_y = (c0[2] + c0[3] + c1[2] + c1[3]) - (c0[0] + c0[1] + c1[0] + c1[1]);
    float grad_z = (c1[0] + c1[1] + c1[2] + c1[3]) - (c0[0] + c0[1] + c0[2] + c0[3]);
    return vec3(grad_x, grad_y, grad_z) * 0.25;
}

#if defined(VERTEX)

layout(location = 0) flat out int out_instanceID;

void main()
{
    // Pass-through
    out_instanceID = gl_InstanceID;
}

#elif defined(TESS_CONTROL)

layout(vertices = 1) out;

layout(location = 0) flat in int in_instanceID[];

layout(location = 0) flat out int out_instanceID[];
layout(location = 1) flat out uvec4 out_brick[];

void main()
{
    int brick_id = gl_PrimitiveID;
    uvec4 brick = texelFetch(u_bricks, brick_id);
    int count = int(brick.y);  // Number of clusters to draw
    int level = int(brick.z);
    vec3 grid_center = vec3((brick.www >> uvec3(0u, 8u, 16u)) & 255u);

    mat4 view_from_local = u_scene.view * u_drawable.model[in_instanceID[gl_InvocationID]];
    vec4 view_pos = view_from_local * vec4(grid_center * exp2(level), 1.0);
    float view_radius = 0.5 * length(view_from_local[0].xyz);
    float point_size = (view_radius / length(view_pos.xyz)) * u_scene.proj[1][1] * u_scene.res[1];
    int lod = clamp(int(floor(log2(u_lod_threshold_pixels / point_size))), 0, 4);

    out_brick[gl_InvocationID] = brick;
    out_instanceID[gl_InvocationID] = in_instanceID[gl_InvocationID];
    gl_TessLevelOuter[0] = level == lod ? clamp(count, 0, 64) : 0;
    gl_TessLevelOuter[1] = CLUSTER_SIZE - 1;
}

#elif defined(TESS_EVALUATION)

layout(isolines, point_mode) in;

layout(location = 0) flat in int in_instanceID[];
layout(location = 1) flat in uvec4 in_brick[];

layout(location = 0) flat out ivec3 out_cluster_info;
layout(location = 1) flat out vec3 out_view_normal;
layout(location = 2) flat out uvec2 out_cell_data;
layout(location = 3) flat out mat3 out_tbn;

void main()
{
    int brick_id = gl_PrimitiveID;
    int first = int(in_brick[0].x);  // Offset to first cluster
    int level = int(in_brick[0].z);

    int cluster_id = first + int(round(gl_TessCoord.y * gl_TessLevelOuter[0]));

    int visibility_id = brick_id + (in_instanceID[0] * MAX_CLUSTERS_BRICK);
    int visibility_bit = visibility_id & 31;
    uint visibility = texelFetch(u_visibility, visibility_id >> 5).r & (1u << visibility_bit);

    int cell_id = int(round(gl_TessCoord.x * gl_TessLevelOuter[1]));
    uvec4 cell = texelFetch(u_cells, cluster_id * CLUSTER_SIZE + cell_id);
    if (all(equal(uvec4(0u), cell))) DISCARD_VERTEX;
    vec4 c0 = unpackUnorm4x8(cell.z);
    vec4 c1 = unpackUnorm4x8(cell.w);
    float lod_scale = exp2(level);
    vec3 local_pos = (vec3((cell.xxx >> uvec3(0u, 11u, 22u)) & 2047u) + 0.5) * lod_scale;
    vec3 local_normal = -normalize(cellGrad(c0, c1));

    // Check if point should be projected onto the zero-distance isosurface
    if (u_point_shape == SHAPE_ROUND || u_point_shape == SHAPE_DISK || visibility == 0u) {
        float local_dist = (cellLinear(vec3(0.5), c0, c1) - ISOVALUE) * 2.0 * 12.0 * lod_scale;
        local_pos.xyz += local_dist * local_normal;
    }

    mat4 view_from_local = u_scene.view * u_drawable.model[in_instanceID[0]];
    vec4 view_pos = view_from_local * vec4(local_pos, 1.0);
    vec3 view_normal = normalize(mat3(view_from_local) * local_normal);
    float view_radius = 0.5 * length(view_from_local[0].xyz) * lod_scale;
    if (dot(view_pos.xyz, view_normal) > 0.0) DISCARD_VERTEX;

    gl_PointSize = (-view_radius / view_pos.z) * u_scene.proj[1][1] * u_scene.res[1];
    float point_scale = u_point_scale * length(view_pos.xyz / view_pos.z);
    gl_PointSize *= point_scale;
    if (visibility == 0u) gl_PointSize = 1.0;
    gl_Position = u_scene.proj * view_pos;

    // Snap point size to nearest 1/8th pixel, to avoid sampling artifacts
    // caused by GL_POINT_SIZE_GRANULARITY limit
    gl_PointSize = ceil(gl_PointSize * 8.0) / 8.0;

    out_cluster_info = ivec3(visibility_id, visibility, level);
    out_view_normal = view_normal;
    out_cell_data = cell.zw;
    mat3 tbn = transpose(mat3(view_from_local) / length(view_from_local[0]));
    out_tbn = point_scale * tbn;
    out_tbn[2] = tbn * (view_pos.xyz / view_radius);
}

#elif defined(FRAGMENT)

layout(location = 0) flat in ivec3 in_cluster_info;
layout(location = 1) flat in vec3 in_view_normal;
layout(location = 2) flat in uvec2 in_cell_data;
layout(location = 3) flat in mat3 in_tbn;

layout(location = 0) out vec4 out_gbuffer;
layout(location = 1) out vec2 out_id;

bool intersectUnitBox(vec3 ray_origin, vec3 ray_dir, out float tmin, out float tmax)
{
    // Branchless ray/AABB intersection test adapted from
    // https://tavianator.com/fast-branchless-raybounding-box-intersections/
    vec3 t1 = (-0.5 - 1e-3 - ray_origin) * clamp(1.0 / ray_dir, -9999.0, 9999.0);
    vec3 t2 = (0.5 + 1e-3 - ray_origin) * clamp(1.0 / ray_dir, -9999.0, 9999.0);
    tmin = max(min(t1[0], t2[0]), max(min(t1[1], t2[1]), min(t1[2], t2[2])));
    tmax = min(max(t1[0], t2[0]), min(max(t1[1], t2[1]), max(t1[2], t2[2])));
    return (tmax - tmin) > 0.0;
}

bool intersectCell(vec3 ray_origin, vec3 ray_dir, float tmin, float tmax)
{
    vec3 p0 = ray_origin + tmin * ray_dir + 0.5;
    vec3 p1 = ray_origin + tmax * ray_dir + 0.5;
    vec3 p2 = (p0 + p1) * 0.5;
    vec4 c0 = unpackUnorm4x8(in_cell_data[0]);
    vec4 c1 = unpackUnorm4x8(in_cell_data[1]);
    float s0 = cellLinear(p0, c0, c1);
    float s1 = cellLinear(p1, c0, c1);
    float s2 = cellLinear(p2, c0, c1);
    return s0 < ISOVALUE && max(s1, s2) >= ISOVALUE;
}

void main()
{
    int visibility_id = in_cluster_info.x;
    int visibility = in_cluster_info.y;
    int level = in_cluster_info.z;

    bool hit = false;
    if (u_point_shape == SHAPE_SQUARE || visibility == 0) {
        hit = true;
    } else if (u_point_shape == SHAPE_ROUND) {
        hit = length(gl_PointCoord - 0.5) < 0.5;
    } else if (u_point_shape == SHAPE_DISK) {
        vec2 xy = (gl_PointCoord * 2.0 - 1.0) * vec2(1.0, -1.0);
        float dz = dot(-in_view_normal.xy / (abs(in_view_normal.z) + 0.2), xy);
        hit = length(vec3(xy, dz)) <= 1.0;
    } else if (u_point_shape == SHAPE_BOX || u_point_shape == SHAPE_CELL) {
        vec2 xy = gl_PointCoord - 0.5;
        vec3 ray_origin = xy.x * in_tbn[0] - xy.y * in_tbn[1];
        vec3 ray_dir = in_tbn[2] + 2.0 * ray_origin;

        float tmin, tmax;
        hit = intersectUnitBox(ray_origin, ray_dir, tmin, tmax);
        if (u_point_shape == SHAPE_CELL)
            hit = hit && intersectCell(ray_origin, ray_dir, tmin, tmax);
    }
    if (!hit) discard;

    out_gbuffer = vec4(in_view_normal, 1.0) * 0.1;
    out_id = intBitsToFloat(ivec2(visibility_id + 1, level));
}

#endif
