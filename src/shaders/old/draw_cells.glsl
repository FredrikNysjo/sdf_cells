#define META_SIZE 8
#define CLUSTER_SIZE 16
#define MAX_CLUSTERS (1 << 20)
#define USE_VOXEL_RASTERISATION 1
#define USE_CELL_RASTERISATION 1
#define DISCARD_VERTEX { gl_Position = vec4(1.0, 1.0, 1.0, 0.0); return; }

#define SHAPE_SQUARE 0
#define SHAPE_ROUND 1
#define SHAPE_BOX 2
#define SHAPE_CELL 3

layout(binding = 0, std140) uniform SceneUniforms {
    mat4 view;
    mat4 proj;
    vec2 res;
    float time;
    int frame;
} u_scene;

layout(binding = 1, std140) uniform DrawableUniforms {
    mat4 model;
    mat4 normal;
} u_drawable;

layout(location = 0) uniform int u_level;
layout(location = 1) uniform int u_point_shape;
layout(location = 2) uniform float u_point_scale;
layout(binding = 0) uniform samplerBuffer u_cells;
layout(binding = 1) uniform usamplerBuffer u_metacells;
layout(binding = 3) uniform usamplerBuffer u_visibility;

vec3 lds_r3(float n)
{
    float phi = 1.2207440846;
    return fract(n / vec3(phi, phi * phi, phi * phi * phi));
}

float cellLinear(vec3 p, vec4 c0, vec4 c1)
{
    return mix(mix(mix(c0[0], c0[1], p.x), mix(c0[2], c0[3], p.x), p.y),
               mix(mix(c1[0], c1[1], p.x), mix(c1[2], c1[3], p.x), p.y), p.z);
}

vec3 cellGrad(vec4 c0, vec4 c1)
{
    float grad_x = (c0[1] + c0[3] + c1[1] + c1[3]) - (c0[0] + c0[2] + c1[0] + c1[2]);
    float grad_y = (c0[2] + c0[3] + c1[2] + c1[3]) - (c0[0] + c0[1] + c1[0] + c1[1]);
    float grad_z = (c1[0] + c1[1] + c1[2] + c1[3]) - (c0[0] + c0[1] + c0[2] + c0[3]);
    return vec3(grad_x, grad_y, grad_z) * 0.25;
}

#if defined(VERTEX)

void main()
{
    // Pass-through
}

#elif defined(TESS_CONTROL)

layout(vertices = 1) out;

void main()
{
    int cluster_id = gl_PrimitiveID;
    int cluster_bit = cluster_id & 31;
    uint visibility = texelFetch(u_visibility, cluster_id >> 5).r & (1u << cluster_bit);

    gl_TessLevelOuter[1] = (u_level > 0)
                         ? ((visibility > 0) ? 15 : 0)
                         : ((visibility > 0) ? 0 : 15);
    gl_TessLevelOuter[0] = u_level > 0 ? 8 : 1;
}

#elif defined(TESS_EVALUATION)

layout(isolines, point_mode) in;

layout(location = 0) flat out ivec2 v_cluster;
layout(location = 1) flat out vec3 v_view_normal;
layout(location = 2) flat out uvec2 v_cell;
layout(location = 3) flat out vec3 v_view_pos;
layout(location = 4) flat out mat3 v_tbn;

void main()
{
    int idx2 = int(round(gl_TessCoord.x * gl_TessLevelOuter[1]));
    int idx = int(round(gl_TessCoord.y * gl_TessLevelOuter[0]));

    uvec3 metacell = texelFetch(u_metacells, gl_PrimitiveID * CLUSTER_SIZE + idx2).rgb;
    vec3 local_pos = vec3((metacell.xxy >> uvec3(0u, 16u, 0u)) & 65535u) * 2.0 + 0.5;
    uint mask = (metacell.y >> 16u) & 255u;
    int cell_id = u_level > 0 ? idx : (gl_PrimitiveID + u_scene.frame) % META_SIZE;
    int cell_offset = int(metacell.z + bitCount(mask & ~(0xffu << cell_id)));

    if ((mask & (1u << cell_id)) == 0u) DISCARD_VERTEX;

    vec4 c0 = (u_level > 0) ? texelFetch(u_cells, cell_offset * 2 + 0) : vec4(0.0);
    vec4 c1 = (u_level > 0) ? texelFetch(u_cells, cell_offset * 2 + 1) : vec4(0.0);
    c0 = clamp(c0, 0.45f, 0.55f); c1 = clamp(c1, 0.45f, 0.55f);  // XXX
    vec3 local_normal = -normalize(cellGrad(c0, c1));
    local_pos += vec3((uvec3(cell_id) >> uvec3(0u, 1u, 2u)) & 1u);
    if (u_level == 0)
        local_pos += lds_r3((gl_PrimitiveID + cell_id + u_scene.frame) & 1023) - 0.5;

    vec4 view_pos = u_scene.view * (u_drawable.model * vec4(local_pos, 1.0));
    vec3 view_normal = normalize(mat3(u_scene.view) * local_normal);
    float view_radius = 0.5 * length(u_drawable.model[0].xyz);
    if (u_level == 1 && u_point_shape == SHAPE_ROUND) {
        float coverage = dot(vec4(0.125), c0 + c1);
        float view_dist = (coverage - 0.5) * view_radius * 4.0 * 7.5;
        view_pos.xyz += view_dist * view_normal;
    }
#if 0
    bool front_facing = u_level == 0 || dot(normalize(view_pos.xyz), view_normal) < 0.0;
    bool visible = front_facing && view_pos.z < 0.0;
    if (!visible) DISCARD_VERTEX;
#endif

    gl_PointSize = (-view_radius / view_pos.z) * u_scene.proj[1][1] * u_scene.res[1];
    gl_PointSize *= u_point_scale;
    if (u_level == 0)
        gl_PointSize = min(1.0, gl_PointSize);
    gl_Position = u_scene.proj * view_pos;

    v_cluster = ivec2(gl_PrimitiveID, 0);
    v_view_normal = view_normal;
    v_view_pos = vec3(view_pos);
    v_cell = uvec2(packUnorm4x8(c0), packUnorm4x8(c1));
    mat3 tbn = transpose(mat3(u_scene.view));
    v_tbn = u_point_scale * tbn;
    v_tbn[2] = tbn * (view_pos.xyz / view_radius);
}

#elif defined(FRAGMENT)

layout(location = 0) flat in ivec2 v_cluster;
layout(location = 1) flat in vec3 v_view_normal;
layout(location = 2) flat in uvec2 v_cell;
layout(location = 3) flat in vec3 v_view_pos;
layout(location = 4) flat in mat3 v_tbn;

layout(location = 0) out vec4 rt_gbuffer;
layout(location = 1) out vec2 rt_id;

bool intersectUnitBox(vec3 ray_origin, vec3 ray_dir, out float tmin, out float tmax)
{
    // Branchless ray/AABB intersection test adapted from
    // https://tavianator.com/fast-branchless-raybounding-box-intersections/
    vec3 t1 = (-0.5 - 1e-2 - ray_origin) * clamp(1.0 / ray_dir, -9999.0, 9999.0);
    vec3 t2 = (0.5 + 1e-2 - ray_origin) * clamp(1.0 / ray_dir, -9999.0, 9999.0);
    tmin = max(min(t1[0], t2[0]), max(min(t1[1], t2[1]), min(t1[2], t2[2])));
    tmax = min(max(t1[0], t2[0]), min(max(t1[1], t2[1]), max(t1[2], t2[2])));
    return (tmax - tmin) > 0.0;
}

// Perform interval refinement from three samples
float intervalRefinement(float tmin, float tmax, float iso, float s0, float s1, float s2)
{
    bool side = step(iso, s0) != step(iso, s2);
    return mix((tmin + tmax) * 0.5, (side ? tmin : tmax), (iso - s2) / ((side ? s0 : s1) - s2));
}

bool intersectCell(vec3 ray_origin, vec3 ray_dir, float tmin, float tmax, out float tout)
{
    vec3 p0 = ray_origin + tmin * ray_dir + 0.5;
    vec3 p1 = ray_origin + tmax * ray_dir + 0.5;
    vec3 p2 = (p0 + p1) * 0.5;
    vec4 c0 = unpackUnorm4x8(v_cell[0]);
    vec4 c1 = unpackUnorm4x8(v_cell[1]);
    float s0 = cellLinear(p0, c0, c1);
    float s1 = cellLinear(p1, c0, c1);
    float s2 = cellLinear(p2, c0, c1);
#if 1
    tout = intervalRefinement(tmin, tmax, 127.0 / 255.0, s0, s1, s2);
#endif
    return s0 < (127.0 / 255.0) && max(s1, s2) >= (127.0 / 255.0);
}

void main()
{
    vec2 point_coord = gl_PointCoord.xy;
    int point_shape = u_point_shape;
    float point_size = 1.0 / dFdx(point_coord.x);

    bool hit = false; gl_FragDepth = gl_FragCoord.z;
    if (point_shape == SHAPE_SQUARE || u_level == 0) {
        hit = true;
    } else if (point_shape == SHAPE_ROUND) {
        hit = length(point_coord - 0.5) < 0.5;
    } else if (point_shape == SHAPE_BOX || point_shape == SHAPE_CELL) {
        vec2 xy = point_coord - 0.5;
        vec3 ray_origin = xy.x * v_tbn[0] - xy.y * v_tbn[1];
        vec3 ray_dir = normalize(v_tbn[2] + 2.0 * ray_origin);

        float tmin, tmax, tout = 0.0;
        hit = intersectUnitBox(ray_origin, ray_dir, tmin, tmax);
        if (point_shape == SHAPE_CELL) {
            hit = hit && intersectCell(ray_origin, ray_dir, tmin, tmax, tout);
        #if 0
            if (hit) {
                float view_radius = length(u_drawable.model[0].xyz);
                vec3 view_dir = normalize(v_view_pos);
                vec4 clip_pos = u_scene.proj * vec4(v_view_pos + (view_radius * tout) * view_dir, 1.0);
                gl_FragDepth = (clip_pos.z / clip_pos.w) * 0.5 + 0.5;
            }
        #endif
        }
    }
    if (!hit) discard;

    rt_gbuffer = vec4(v_view_normal, 1.0) * 0.1;
    rt_id = intBitsToFloat(ivec2(((v_cluster[0] + 1) << 1) | u_level, v_cluster[1]));
}

#endif
