#define META_SIZE 8
#define CLUSTER_SIZE 16
#define MAX_CLUSTERS (1 << 20)
#define USE_VOXEL_RASTERISATION 1
#define USE_CELL_RASTERISATION 1

#define DISCARD_VERTEX { gl_Position = vec4(1.0, 1.0, 1.0, 0.0); return; }

layout(binding = 0, std140) uniform SceneUniforms {
    mat4 view;
    mat4 proj;
    vec2 res;
    float time;
    int frame;
} u_scene;

layout(binding = 1, std140) uniform DrawableUniforms {
    mat4 model;
    mat4 normal;
} u_drawable;

layout(binding = 0, std430) restrict buffer DrawBuffer {
    ivec4 cmds[4];
    ivec2 data[];
} u_draw_buffer;

layout(location = 2) uniform float u_point_scale;
layout(binding = 0) uniform usamplerBuffer u_cells;
layout(binding = 1) uniform usamplerBuffer u_metacells;
layout(binding = 2) uniform sampler2D u_depth;

vec3 cellGrad(vec4 c0, vec4 c1)
{
    float grad_x = (c0[1] + c0[3] + c1[1] + c1[3]) - (c0[0] + c0[2] + c1[0] + c1[2]);
    float grad_y = (c0[2] + c0[3] + c1[2] + c1[3]) - (c0[0] + c0[1] + c1[0] + c1[1]);
    float grad_z = (c1[0] + c1[1] + c1[2] + c1[3]) - (c0[0] + c0[1] + c0[2] + c0[3]);
    return vec3(grad_x, grad_y, grad_z) * 0.25;
}

#if defined(VERTEX)

layout(location = 0) flat out vec3 v_normal;
layout(location = 1) flat out float v_max_depth;

void main()
{
    int stride = (META_SIZE) * CLUSTER_SIZE;
    ivec2 cluster = u_draw_buffer.data[1 * MAX_CLUSTERS + gl_VertexID / stride];
    int meta_id = (gl_VertexID / META_SIZE) % CLUSTER_SIZE;
    int cell_id = (gl_VertexID) % META_SIZE;
    int meta_offset = cluster[0] * CLUSTER_SIZE + meta_id;
    int cell_offset = cluster[1];

    uvec2 metacell = texelFetch(u_metacells, meta_offset).rg;
    vec3 meta_local_pos = vec3((metacell.xxx >> uvec3(0u, 11u, 22u)) & 2047u) * 2.0 + 0.5;
    uint mask = (metacell.y >> 24u);
    cell_offset += int((metacell.y & 0xffffffu) + bitCount(mask & ~(0xffu << cell_id)));

    if ((mask & (1u << cell_id)) == 0u) DISCARD_VERTEX;

    uvec2 cell = texelFetch(u_cells, cell_offset).rg;
    vec4 c0 = unpackUnorm4x8(cell[0]);
    vec4 c1 = unpackUnorm4x8(cell[1]);
    vec3 local_normal = -normalize(cellGrad(c0, c1));
    vec3 local_pos = meta_local_pos + vec3((uvec3(cell_id) >> uvec3(0u, 1u, 2u)) & 1u);

    vec4 view_pos = u_scene.view * (u_drawable.model * vec4(local_pos, 1.0));
    vec3 view_normal = normalize(mat3(u_scene.view) * local_normal);
    float view_radius = 0.5 * length(u_drawable.model[0].xyz);
    bool front_facing = dot(normalize(view_pos.xyz), view_normal) < 0.01;
    bool visible = front_facing && view_pos.z < 0.0;

    float point_size = (-view_radius / view_pos.z) * u_scene.proj[1][1] * u_scene.res[1];
    point_size *= u_point_scale * 1.3;
    gl_PointSize = max(0.5, point_size);

    vec4 clip_pos = u_scene.proj * vec4(view_pos.xyz + normalize(view_pos.xyz) * view_radius * 4.0, 1.0);
    view_pos.xyz += -normalize(view_pos.xyz) * view_radius * 4.0;
    gl_Position = u_scene.proj * view_pos;
    if (!visible || point_size < 0.8 * fract((cell_id + META_SIZE * meta_id) * 1.618034))
        DISCARD_VERTEX;

    v_normal = view_normal;
    v_max_depth = (clip_pos.z / clip_pos.w) * 0.5 + 0.5;
}

#elif defined(FRAGMENT)

layout(location = 0) flat in vec3 v_normal;
layout(location = 1) flat in float v_max_depth;
layout(location = 0) out vec4 rt_gbuffer;

void main()
{
    float fade = max(0.1, 1.0 - length(gl_PointCoord - 0.5) * 2.0);
    if (texelFetch(u_depth, ivec2(gl_FragCoord.xy), 0).r > v_max_depth) fade = 0.0;
    rt_gbuffer = vec4(v_normal, 1.0) * fade;
}

#endif
