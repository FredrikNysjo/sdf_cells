layout(binding = 0) uniform sampler2D u_color;
layout(binding = 1, rgba16f) restrict uniform image2D u_history;

layout(location = 0) uniform int u_frame;

#if defined(COMPUTE)

layout(local_size_x = 8, local_size_y = 8) in;

void main()
{
    ivec2 frag_pos = ivec2(gl_WorkGroupID.xy * gl_WorkGroupSize.xy + gl_LocalInvocationID.xy);

    vec4 m1 = vec4(0.0);
    vec4 m2 = vec4(0.0);
    for (int j = -1; j <= 1; ++j) {
        for (int i = -1; i <= 1; ++i) {
            vec4 color = texelFetch(u_color, frag_pos + ivec2(i, j), 0);
            m1 += color;
            m2 += color * color;
        }
    }

    vec4 mu = m1 * (1.0 / 9.0);
    vec4 sigma = sqrt(abs(m2 * (1.0 / 9.0) - mu * mu));

    vec4 color = texelFetch(u_color, frag_pos, 0);
    vec4 history = imageLoad(u_history, frag_pos);
    vec4 history_clipped = clamp(history, mu - sigma, mu + sigma);
    vec4 output_color = mix(history_clipped, color, 0.1);
    if (u_frame > 0)
        output_color = mix(history, color, 1.0 / u_frame);

    imageStore(u_history, frag_pos, output_color);
}

#endif
