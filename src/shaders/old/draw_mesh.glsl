#define USE_SNORM16_VERTEX_FORMAT 1

layout(binding = 0, std140) uniform SceneUniforms {
    mat4 view;
    mat4 projection;
    vec2 resolution;
    float time;
    int frame;
} u_scene;

layout(binding = 1, std140) uniform DrawableUniforms {
    mat4 model;
    mat4 normal;
} u_drawable;

layout(location = 0) uniform float u_splat_scale;
layout(location = 1) uniform int u_shadows_enabled;
layout(location = 2) uniform mat4 u_shadow_from_view;
#if USE_SNORM16_VERTEX_FORMAT
layout(binding = 0) uniform usamplerBuffer u_vertices;
#else // USE_SNORM16_VERTEX_FORMAT
layout(binding = 0) uniform samplerBuffer u_vertices;
#endif // USE_SNORM16_VERTEX_FORMAT
layout(binding = 0, r32ui) restrict uniform uimage2D u_shadowmap;

void shadowmapSplatDepth(vec3 view_pos)
{
    vec4 clip_pos = u_shadow_from_view * vec4(view_pos, 1.0);
    vec3 ndc_pos = clip_pos.xyz / clip_pos.w;
    vec2 frag_pos = (ndc_pos.xy * 0.5 + 0.5) * vec2(512, 512);
    float frag_depth = ndc_pos.z * 0.5 + 0.5;
    imageAtomicMin(u_shadowmap, ivec2(frag_pos), floatBitsToUint(frag_depth));
}

#if defined(VERTEX)

layout(location = 0) out vec4 v_view_pos;

void main()
{
#if USE_SNORM16_VERTEX_FORMAT
    uvec2 texel = texelFetch(u_vertices, gl_VertexID).rg;
    float scale = unpackSnorm2x16(texel.g).y * 32767.0f;
    vec3 local_pos = vec3(unpackSnorm2x16(texel.r), unpackSnorm2x16(texel.g).r) * scale - 0.5;
#else // USE_SNORM16_VERTEX_FORMAT
    vec3 local_pos = texelFetch(u_vertices, gl_VertexID).rgb - 0.5;
#endif // USE_SNORM16_VERTEX_FORMAT

    vec4 view_pos = u_scene.view * (u_drawable.model * vec4(local_pos, 1.0));
    float view_radius = 0.5 * length(u_drawable.model[0].xyz);
    float point_size = (-view_radius / view_pos.z) * u_scene.projection[1][1] * u_scene.resolution[1];
    point_size *= u_splat_scale;

    if (bool(u_shadows_enabled)) {
        shadowmapSplatDepth(view_pos.xyz);
    }

    v_view_pos = vec4(view_pos.xyz, point_size);
    gl_Position = u_scene.projection * view_pos;
}

#elif defined(FRAGMENT)

layout(location = 0) in vec4 v_view_pos;
layout(location = 0) out vec4 rt_gbuffer;
layout(location = 1) out float rt_id;

void main()
{
    // Compute view normal from fragment group
    vec3 T = normalize(dFdx(v_view_pos.xyz));
    vec3 B = normalize(dFdy(v_view_pos.xyz));
    vec3 N = normalize(cross(T, B));

    rt_gbuffer = vec4(N, v_view_pos.w /*point_size*/);
    rt_id = intBitsToFloat((1 << 1) | 1);
}

#endif
