#define META_SIZE 8
#define CLUSTER_SIZE 16
#define MAX_CLUSTERS (1 << 20)

layout(binding = 0) uniform sampler2D u_id;
layout(binding = 0, r32ui) restrict uniform uimageBuffer u_visibility;
layout(binding = 1, r32ui) restrict uniform uimageBuffer u_visibility2;

#if defined(COMPUTE)

layout(local_size_x = 8, local_size_y = 8) in;

shared int g_cluster[64];
shared int g_level[64];

void main()
{
    vec2 frag_pos = vec2(gl_WorkGroupID.xy * gl_WorkGroupSize.xy + gl_LocalInvocationID.xy) + 0.5;

    vec2 texel = texelFetch(u_id, ivec2(frag_pos), 0).rg;
    int cluster_id = (floatBitsToInt(texel.r) >> 1) - 1;
    int level = floatBitsToInt(texel.r) & 1;

    g_cluster[gl_LocalInvocationIndex] = cluster_id;
    g_level[gl_LocalInvocationIndex] = level;
    barrier();

    if (gl_LocalInvocationIndex % 4 > 0)
        return;

    int last = -1;
    for (int i = 0; i < 4; ++i) {
        cluster_id = g_cluster[gl_LocalInvocationIndex + i];
        level = g_level[gl_LocalInvocationIndex + i];

        if (cluster_id >= 0 && cluster_id != last) {
            int cluster_bit = cluster_id % 32;
            uint mask = 1u << cluster_bit;
            imageAtomicOr(u_visibility, cluster_id / 32, mask);
            if (level == 0)
                imageAtomicOr(u_visibility2, cluster_id / 32, mask);
        }
        last = cluster_id;
    }
}

#endif
