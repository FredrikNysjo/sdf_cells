#define META_SIZE 8
#define CLUSTER_SIZE 16
#define MAX_CLUSTERS (1 << 20)
#define USE_VOXEL_RASTERISATION 1
#define USE_CELL_RASTERISATION 1

#define DISCARD_VERTEX { gl_Position = vec4(1.0, 1.0, 1.0, 0.0); return; }

layout(binding = 0, std140) uniform SceneUniforms {
    mat4 view;
    mat4 proj;
    vec2 res;
    float time;
    int frame;
} u_scene;

layout(binding = 1, std140) uniform DrawableUniforms {
    mat4 model;
    mat4 normal;
} u_drawable;

layout(binding = 0, std430) restrict buffer DrawBuffer {
    ivec4 cmds[4];
    ivec2 data[];
} u_draw_buffer;

layout(location = 0) uniform int u_level;
layout(location = 2) uniform float u_point_scale;
layout(location = 3) uniform int u_shadows_enabled;
layout(location = 4) uniform mat4 u_shadow_from_view;
layout(binding = 0) uniform usamplerBuffer u_cells;
layout(binding = 1) uniform usamplerBuffer u_metacells;
layout(binding = 0, r32ui) uniform restrict uimage2D u_shadowmap;

vec3 lds_r3(float n)
{
    float phi = 1.2207440846;
    return fract(n / vec3(phi, phi * phi, phi * phi * phi));
}

float cellLinear(vec3 p, vec4 c0, vec4 c1)
{
    return mix(mix(mix(c0[0], c0[1], p.x), mix(c0[2], c0[3], p.x), p.y),
               mix(mix(c1[0], c1[1], p.x), mix(c1[2], c1[3], p.x), p.y), p.z);
}

vec3 cellGrad(vec4 c0, vec4 c1)
{
    float grad_x = (c0[1] + c0[3] + c1[1] + c1[3]) - (c0[0] + c0[2] + c1[0] + c1[2]);
    float grad_y = (c0[2] + c0[3] + c1[2] + c1[3]) - (c0[0] + c0[1] + c1[0] + c1[1]);
    float grad_z = (c1[0] + c1[1] + c1[2] + c1[3]) - (c0[0] + c0[1] + c0[2] + c0[3]);
    return vec3(grad_x, grad_y, grad_z) * 0.25;
}

void splatShadowDepth(vec3 local_pos)
{
    vec4 clip_pos = u_shadow_from_view * (u_scene.view * (u_drawable.model * vec4(local_pos, 1.0)));
    vec3 ndc_pos = clip_pos.xyz / clip_pos.w;
    vec2 frag_pos = (ndc_pos.xy * 0.5 + 0.5) * vec2(512, 512);
    float frag_depth = ndc_pos.z * 0.5 + 0.5;
    imageAtomicMin(u_shadowmap, ivec2(frag_pos), floatBitsToUint(frag_depth));
}

#if defined(VERTEX)

layout(location = 0) flat out ivec2 v_cluster;
layout(location = 1) flat out vec3 v_view_normal;
layout(location = 2) flat out uvec2 v_cell;
layout(location = 3) flat out mat3 v_tbn;

void main()
{
    int stride = (u_level > 0 ? META_SIZE : 1) * CLUSTER_SIZE;
    ivec2 cluster = u_draw_buffer.data[u_level * MAX_CLUSTERS + gl_VertexID / stride];
    int meta_id = (u_level > 0 ? gl_VertexID / META_SIZE : gl_VertexID) % CLUSTER_SIZE;
    int cell_id = (u_level > 0 ? gl_VertexID : u_scene.frame + meta_id) % META_SIZE;
    int meta_offset = cluster[0] * CLUSTER_SIZE + meta_id;
    int cell_offset = cluster[1];

    uvec2 metacell = texelFetch(u_metacells, meta_offset).rg;
    vec3 meta_local_pos = vec3((metacell.xxx >> uvec3(0u, 11u, 22u)) & 2047u) * 2.0 + 0.5;
    uint mask = (metacell.y >> 24u);
    cell_offset += int((metacell.y & 0xffffffu) + bitCount(mask & ~(0xffu << cell_id)));

    if (bool(u_shadows_enabled) && (u_level == 0 || cell_id == 0) && mask > 0u)
        splatShadowDepth(meta_local_pos + 1.0);

    if ((mask & (1u << cell_id)) == 0u)
        DISCARD_VERTEX;

    uvec2 cell = (u_level > 0) ? texelFetch(u_cells, cell_offset).rg : uvec2(0u);
    vec4 c0 = unpackUnorm4x8(cell[0]);
    vec4 c1 = unpackUnorm4x8(cell[1]);
    vec3 local_normal = -normalize(cellGrad(c0, c1));
    vec3 local_pos = meta_local_pos + vec3((uvec3(cell_id) >> uvec3(0u, 1u, 2u)) & 1u);
    if (u_level == 0)
        local_pos += lds_r3((cluster[0] + cell_id + u_scene.frame) & 1023) - 0.5;

    vec4 view_pos = u_scene.view * (u_drawable.model * vec4(local_pos, 1.0));
    vec3 view_normal = normalize(mat3(u_scene.view) * local_normal);
    float view_radius = 0.5 * length(u_drawable.model[0].xyz);
    bool front_facing = u_level == 0 || dot(normalize(view_pos.xyz), view_normal) < 0.01;
    bool visible = front_facing && view_pos.z < 0.0;

    float point_size = (-view_radius / view_pos.z) * u_scene.proj[1][1] * u_scene.res[1];
    point_size *= u_point_scale;
    gl_PointSize = max(0.5, (u_level == 0) ? min(1.0, point_size) : point_size);
    gl_Position = u_scene.proj * view_pos;
    if (!visible || point_size < 0.8 * fract((cell_id + META_SIZE * meta_id) * 1.618034))
        DISCARD_VERTEX;

    v_cluster = cluster;
    v_view_normal = view_normal;
    v_cell = cell;
    v_tbn = u_point_scale * transpose(mat3(u_scene.view));
    v_tbn[2] = normalize(v_tbn * view_pos.xyz);
}

#elif defined(FRAGMENT)

layout(location = 0) flat in ivec2 v_cluster;
layout(location = 1) flat in vec3 v_view_normal;
layout(location = 2) flat in uvec2 v_cell;
layout(location = 3) flat in mat3 v_tbn;

layout(location = 0) out vec4 rt_gbuffer;
layout(location = 1) out vec2 rt_id;

// Branchless ray/AABB intersection test adapted from
// https://tavianator.com/fast-branchless-raybounding-box-intersections/
bool intersectUnitBox(vec3 ray_origin, vec3 ray_dir_inv, out float tmin, out float tmax)
{
    vec3 t1 = (-0.5 - ray_origin) * ray_dir_inv;
    vec3 t2 = (0.5 - ray_origin) * ray_dir_inv;
    tmin = max(min(t1[0], t2[0]), max(min(t1[1], t2[1]), min(t1[2], t2[2])));
    tmax = min(max(t1[0], t2[0]), min(max(t1[1], t2[1]), max(t1[2], t2[2])));

    return bool((tmax - tmin) > 0.0);
}

bool intersectCell(vec3 ray_origin, vec3 ray_dir, float tmin, float tmax)
{
    vec3 p0 = clamp(ray_origin + tmin * ray_dir + 0.5, 0.0, 1.0);
    vec3 p1 = clamp(ray_origin + tmax * ray_dir + 0.5, 0.0, 1.0);
    vec4 c0 = unpackUnorm4x8(v_cell[0]);
    vec4 c1 = unpackUnorm4x8(v_cell[1]);
    float s0 = cellLinear(mix(p0, p1, 0.0), c0, c1);
    float s1 = cellLinear(mix(p0, p1, 0.5), c0, c1);
    float s2 = cellLinear(mix(p0, p1, 1.0), c0, c1);

    return max(s0, max(s1, s2)) >= 0.5; // && min(s0, min(s1, s2)) < 0.5;
}

void main()
{
    vec3 view_normal = v_view_normal;
    float point_size = 1.0 / dFdx(gl_PointCoord.x);

#if USE_VOXEL_RASTERISATION
    if (u_level > 0 && point_size > 0.8) {
        vec2 uv = gl_PointCoord - 0.5;
        vec3 ray_origin = uv.x * v_tbn[0] - uv.y * v_tbn[1];
        vec3 ray_dir = v_tbn[2];
        float tmin, tmax;
        bool hit = intersectUnitBox(ray_origin, 1.0 / ray_dir, tmin, tmax);

    #if USE_CELL_RASTERISATION
        hit = hit && intersectCell(ray_origin, ray_dir, tmin, tmax);
    #endif // USE_CELL_RASTERISATION
        if (!hit) discard;
    }
#endif // USE_VOXEL_RASTERISATION

    rt_gbuffer = vec4(view_normal, 1.0) * 0.1;
    rt_id = intBitsToFloat(ivec2(((v_cluster[0] + 1) << 1) | u_level, v_cluster[1]));
}

#endif
