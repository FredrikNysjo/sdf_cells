#define USE_HOLE_FILLING 1

layout(binding = 0) uniform sampler2D u_shadowmap_raw;
layout(binding = 0, r32f) uniform restrict writeonly image2D u_shadowmap;

#if defined(COMPUTE)

layout(local_size_x = 8, local_size_y = 8) in;

void main()
{
    ivec2 frag_pos = ivec2(gl_WorkGroupID.xy * gl_WorkGroupSize.xy + gl_LocalInvocationID.xy);
    float center_texel = texelFetch(u_shadowmap_raw, frag_pos, 0).r;

    float depth_min = center_texel;
#if USE_HOLE_FILLING
    for (int j = -1; j <= 1; ++j) {
        for (int i = -1; i <= 1; ++i) {
            float depth_ij = texelFetch(u_shadowmap_raw, frag_pos + ivec2(i, j), 0).r;
            depth_min = min(depth_ij, depth_min);
        }
    }
#endif // USE_HOLE_FILLING

    imageStore(u_shadowmap, frag_pos, vec4(depth_min));
}

#endif
