#define MAX_GRID_LEVEL 4

layout(binding = 0, std140) uniform SceneUniforms {
    mat4 view_from_world;
    mat4 proj_from_view;
    vec2 resolution;
    float time;
    int frame;
} u_scene;

layout(binding = 1, std430) restrict readonly buffer DrawableBuffer {
    mat4 world_from_local[];
} u_drawable;

layout(location = 0) uniform float u_lod_threshold_pixels;
layout(binding = 1) uniform usamplerBuffer u_bricks;
layout(binding = 2) uniform usamplerBuffer u_lodinfo;
layout(binding = 2, rgba32ui) restrict uniform uimageBuffer u_indirect;

int findLOD(vec3 grid_pos, mat4 view_from_local, float view_radius)
{
    vec4 view_pos = view_from_local * vec4(grid_pos, 1.0);
    float point_size = (view_radius / length(view_pos.xyz)) * u_scene.proj_from_view[1][1] *
                       u_scene.resolution[1];
    float lod = clamp(log2(u_lod_threshold_pixels / point_size), 0, MAX_GRID_LEVEL);
    return int(lod);
}

#if defined(COMPUTE)

layout(local_size_x = 64) in;

void main()
{
    int thread_id = int(gl_GlobalInvocationID.x);

    mat4 view_from_local = u_scene.view_from_world * u_drawable.world_from_local[thread_id];
    float view_radius = 0.5 * length(view_from_local[0].xyz);

    uvec4 lodinfo_base = texelFetch(u_lodinfo, 0);
    int min_lod = MAX_GRID_LEVEL;
    for (int i = 0; i < 8; ++i) {
        vec3 grid_dim = vec3(lodinfo_base.xyz);
        vec3 grid_pos_i = vec3((uvec3(i) >> uvec3(0u, 1u, 2u)) & 1u) * grid_dim;
        min_lod = min(findLOD(grid_pos_i, view_from_local, view_radius), min_lod);
    }

    uvec4 lodinfo = texelFetch(u_lodinfo, min_lod);
    int count = int(lodinfo.w);

    uvec4 cmd = uvec4(count, 1, 0, 0);
    imageStore(u_indirect, thread_id, cmd); 
}

#endif
