#define MAX_CLUSTERS_BRICK 4096
#define SHOW_BACKGROUND 0
#define TWO_PI 6.283184

layout(binding = 0, std140) uniform SceneUniforms {
    mat4 view_from_world;
    mat4 proj_from_view;
    vec2 resolution;
} u_scene;

layout(location = 0) uniform vec3 u_bg_color0;
layout(location = 1) uniform vec3 u_bg_color1;
layout(location = 2) uniform vec3 u_base_color;
layout(location = 3) uniform float u_roughness;
layout(location = 4) uniform int u_show_normals;
layout(location = 5) uniform int u_show_visibility_id;
layout(location = 6) uniform int u_show_cell_id;
layout(location = 7) uniform int u_show_lod;
layout(location = 8) uniform int u_gamma_enabled;
layout(binding = 0) uniform usamplerBuffer u_cells;
layout(binding = 1) uniform usamplerBuffer u_bricks;
layout(binding = 2) uniform sampler2D u_depth;
layout(binding = 1, rgba16f) restrict uniform image2D u_gbuffer;
layout(binding = 2, rg32ui) restrict readonly uniform uimage2D u_id;
layout(binding = 5, rgba16f) restrict writeonly uniform image2D u_color;

vec3 lds_r3(float n)
{
    float phi = 1.2207440846;
    return fract(n / vec3(phi, phi * phi, phi * phi * phi));
}

vec3 reconstructViewPos(vec3 ndc_pos, mat4 proj_from_view)
{
    float a = proj_from_view[0][0];
    float b = proj_from_view[1][1];
    float c = proj_from_view[2][2];
    float d = proj_from_view[3][2];
    float e = proj_from_view[2][0];
    float f = proj_from_view[2][1];
    float view_pos_z = -d / (ndc_pos.z + c);
    float view_pos_x = (-view_pos_z / a) * (ndc_pos.x + e);
    float view_pos_y = (-view_pos_z / b) * (ndc_pos.y + f);
    return vec3(view_pos_x, view_pos_y, view_pos_z);
}

vec3 hemisphereLight(vec3 ray_dir, float gloss)
{
    float t = clamp(ray_dir.y / (1.0 - gloss) + 0.5, 0.0, 1.0);
    return mix(u_bg_color0, u_bg_color1, t);
}

vec3 computeLighting(vec3 view_normal, vec3 view_pos, vec3 albedo, float gloss, vec2 frag_pos)
{
    vec3 N = normalize(view_normal);
    vec3 V = -normalize(view_pos);
    vec3 L = normalize(vec3(0.5, 1.0, 2.0) - view_pos);
    vec3 H = normalize(V + L);
    float N_dot_L = max(0.0, dot(N, L));
    float N_dot_H = max(0.0, dot(N, H));
    float alpha = exp2(10.0 * gloss);
    float normalization = (8.0 + alpha) / 8.0;
    vec3 F0 = vec3(0.04);
    vec3 F_schlick = (F0 + (1.0 - F0) * pow(1.0 - max(0.0, dot(N, V)), 5.0));
    albedo *= (1.0 - F0);

    vec3 color = vec3(0.0);
    color += albedo * 0.8 * N_dot_L;
    color += F0 * normalization * 0.8 * pow(N_dot_H, alpha);
    color += albedo * 0.4 * hemisphereLight(N, 0.0);
    color += F_schlick * 0.4 * hemisphereLight(reflect(N, -V), gloss);
    return color;
}

#if defined(COMPUTE)

layout(local_size_x = 8, local_size_y = 8) in;

void main()
{
    vec2 frag_pos = vec2(gl_WorkGroupID.xy * gl_WorkGroupSize.xy + gl_LocalInvocationID.xy) + 0.5;

    float depth_texel = texelFetch(u_depth, ivec2(frag_pos), 0).r;
    if (depth_texel >= 1.0) {
    #if SHOW_BACKGROUND
        vec3 ndc_pos = vec3(frag_pos / u_scene.resolution.xy, 0.0) * 2.0 - 1.0;
        vec3 view_pos = reconstructViewPos(ndc_pos, u_scene.proj_from_view);
        vec3 sky_color = hemisphereLight(normalize(view_pos), 0.0) * 0.0;
        imageStore(u_color, ivec2(frag_pos), vec4(sky_color, 1.0));
    #endif  // SHOW_BACKGROUND
        return;
    }

    uvec2 id_texel = imageLoad(u_id, ivec2(frag_pos)).rg;
    int visibility_id = int(id_texel.r) - 1;

    vec4 gbuffer_texel = imageLoad(u_gbuffer, ivec2(frag_pos));
    vec3 view_normal = normalize(gbuffer_texel.rgb);
    vec3 albedo = u_base_color * u_base_color;
    float gloss = 1.0 - u_roughness;

    if (bool(u_show_lod)) {
        uvec4 brick = texelFetch(u_bricks, visibility_id % MAX_CLUSTERS_BRICK);
        albedo = lds_r3(float(brick.z) + 2.0);
    }
    if (bool(u_show_visibility_id)) {
        albedo = lds_r3((visibility_id % 123) + 0.5);
    }
    if (bool(u_show_cell_id)) {
        albedo = lds_r3(float(id_texel.g) + 0.5);
    }

    vec3 ndc_pos = vec3(frag_pos / u_scene.resolution.xy, depth_texel) * 2.0 - 1.0;
    vec3 view_pos = reconstructViewPos(ndc_pos, u_scene.proj_from_view);

    vec4 output_color = vec4(1.0);
    output_color.rgb = computeLighting(view_normal, view_pos, albedo, gloss, frag_pos);

    if (bool(u_show_normals)) {
        output_color.rgb = pow(view_normal * 0.5 + 0.5, vec3(2.2));
    }

    imageStore(u_color, ivec2(frag_pos), output_color);
}

#endif
