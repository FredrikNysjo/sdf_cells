# Third-Party Dependencies

Third-party dependencies for a simple rendering framework

## License

gl3w, GLFW, GLM, imgui, and stb are distributed under
their own respective licensens. See `LICENSES.md` for details.
