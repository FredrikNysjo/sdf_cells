# sdf_cells

Work-in-progress demo of grid cell rasterisation of signed-distance-field (SDF) models. Basically a continuation of some of the work that I did in the WSCG paper ["Clustered Grid Cells Data Structure for Isosurface Rendering"](http://wscg.zcu.cz/wscg2020/journal/E07.pdf) and implemented [here](https://bitbucket.org/FredrikNysjo/grid_cells). Features:

- Faster rasterisation of small cells, via 32-bits atomic integer operations
- Instancing support (limit currently set to 2^16 objects in the demo)
- Dynamic level-of-detail (LOD)
- GPU-based frustum and occlusion culling 
- Smooth normal interpolation (via optional attribute blending pass)

Note: in the current demo and implementation, the grid-cells are being rasterised as surfels (but still store eight corner values with information about the local SDF).


## Example screenshot

![Screenshot](screenshot.png?raw=true "Screenshot")


## Build instructions for Linux

You need to install the following tools, which should be available in the distributions package manager:

    gcc/g++, CMake 3.10 (or higher), make, git

In addition, you also need a few libraries from the package manager. On for example Ubuntu 18.04 and 20.04, you can install them by

    sudo apt install libgl1-mesa-dev libxmu-dev libxi-dev libxrandr-dev libxcursor-dev libxinerama-dev

Once the tools and dependencies are installed, set the environment variable `SDF_CELLS_ROOT` to the path where this `README.md` is located, and run the provided `build.sh` script to build and run the demo application. The resulting binary `sdf_cells` will be placed under `SDF_CELLS_ROOT/bin`.


## Build instructions for Windows

On Windows, you can install VSCode and the Microsoft Visual C++ (MSVC) compiler via the instructions from https://code.visualstudio.com/docs/cpp/config-msvc , and then use CMake (3.14 or higher) from the command line or via the [VSCode Tools addon](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cmake-tools) to generate and build the demo application. Before running the application, you also need to set the environment variable `SDF_CELLS_ROOT` to the path where this `README.md` is located

The provided `build.sh` script can also be used from Git Bash on Windows, provided that you have CMake and the MSVC compiler installed. To change the CMake generator to match the MSVC version, you can use the `SDF_CELLS_GENERATOR` environment variable (by default, this variable is set to "Visual Studio 16 2019" in the script).


## Usage from the command line

Command line usage on Linux:

    ./sdf_cells [vtk_filename]

Command line usage on Windows:

    sdf_cells.exe [vtk_filename]

VTK-files can also be loaded by dragging and dropping them onto the running application window.


## Sample data

A few sample models (stored in VTK-format) can be found under the `data` folder.


## Input controls

- Mouse scroll wheel: Zoom camera in/out
- Mouse right button: Rotate camera
- Mouse middle button: Pan camera


## Third-party dependencies

The application depends on the following third-party libraries, which are included in the `third-party` folder and built from source code during compilation:

- gl3w (http://github.com/skaslev/gl3w)
- GLFW v3.3.2 (https://github.com/glfw/glfw)
- GLM v0.9.9.8 (https://github.com/g-truc/glm)
- ImGui v1.79 (https://github.com/ocornut/imgui)
- stb_image.h v2.26 (https://github.com/nothings/stb)


## Code style

This code uses the WebKit C++ style (with minor modifications) and clang-format (version 6.0) for automatic formatting.


## License

The code is provided under the MIT license (see `LICENSE.md`).
