#!/bin/bash

if [ -z "$SDF_CELLS_ROOT" ]; then
    echo "SDF_CELLS_ROOT is not set"
    exit 1
fi

if [ -z "$SDF_CELLS_GENERATOR" ]; then
    SDF_CELLS_GENERATOR="Unix Makefiles"
    if [[ "$OSTYPE" == "msys" ]]; then
        SDF_CELLS_GENERATOR="Visual Studio 16 2019"
    fi
fi

# Generate build files
if [ ! -d build ]; then
    mkdir build
fi
cd build && \
cmake -G "$SDF_CELLS_GENERATOR" ../ -DCMAKE_INSTALL_PREFIX=../ && \

# Build and install the program
cmake --build . --config RelWithDebInfo --target install && \

# Run the program
cd ../bin && \
`find . -executable -type f`
