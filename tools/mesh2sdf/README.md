# mesh2sdf

This tool generates a signed distance field (SDF) volume from an OBJ-mesh, and
outputs it as a VTK structured grid file.

Author: Fredrik Nysjö (2021)


## Build instructions for Linux

You need the following tools, which can be installed via the package manager of your Linux distribution:

    gcc/g++, CMake 3.5 (or higher), make, git

To build the code, run the included `build.sh` script from a terminal. The script will also run the resulting binary, which should generate a file `output.vtk` for one of the meshes included in the `models` folder.


## Build instructions for Windows

We recommend using Visual Studio Community 2019 and CMake GUI 3.14 (or higher) for building the code on Windows. Using WSL on Windows 10 has not been tested, but should work if you follow the Linux build instructions above.


## Basic usage

    mesh2sdf input.obj [output.vtk]


## Third-party dependencies

The tool depends on GLM, which is included in the `../../third_party` folder.


### Code style

This code uses the WebKit C++ style (with minor modifications) and clang-format (version 6.0) for automatic formatting.
