#pragma once

#include <cstdio>
#include <cstdlib>
#include <vector>
#include <chrono>

#undef NDEBUG
#include <cassert>

enum OBJMeshLayout {
    OBJ_MESH_LAYOUT_AOS_XYZ,
};

struct OBJMesh {
    std::vector<float> vertices;
    std::vector<int> indices;
    int layout;
};

struct STLMesh {
    std::vector<float> vertices;
    int layout;
};

double time()
{
    static auto start = std::chrono::high_resolution_clock::now();
    auto now = std::chrono::high_resolution_clock::now();

    return std::chrono::duration<double>(now - start).count();
}

void write_ppm(const char *fname, const int *image, int w, int h)
{
    FILE *stream = std::fopen(fname, "wb+");
    if (!stream) {
        std::fprintf(stderr, "Could not open %s\n", fname);
        std::exit(EXIT_FAILURE);
    }

    std::fprintf(stream, "P6\n%d %d\n255\n", w, h);
    for (int i = 0; i < (w * h); ++i) {
        unsigned char r = image[i] & 0xff;
        unsigned char g = (image[i] >> 8) & 0xff;
        unsigned char b = (image[i] >> 16) & 0xff;
        std::fprintf(stream, "%c%c%c", r, g, b);
    }
    std::fclose(stream);
}

void read_obj(const char *fname, OBJMesh &mesh)
{
    FILE *stream = std::fopen(fname, "rb");
    if (!stream) {
        std::fprintf(stderr, "Could not open %s\n", fname);
        std::exit(EXIT_FAILURE);
    }

    char line[256] = {0};
    while (std::fgets(line, 256, stream)) {
        float x, y, z;
        if ((line[0] == 'v') && (std::sscanf(line, "v %f %f %f", &x, &y, &z) == 3)) {
            mesh.vertices.push_back(x);
            mesh.vertices.push_back(y);
            mesh.vertices.push_back(z);
            continue;
        }
        int i0, i1, i2;
        if ((line[0] == 'f') && (std::sscanf(line, "f %d %d %d", &i0, &i1, &i2) == 3)) {
            mesh.indices.push_back(i0 - 1);
            mesh.indices.push_back(i1 - 1);
            mesh.indices.push_back(i2 - 1);
        }
        // Ignore other lines
    }
    std::fclose(stream);
}

void read_stl(const char *fname, STLMesh &mesh)
{
    FILE *stream = std::fopen(fname, "rb");
    if (!stream) {
        std::fprintf(stderr, "Could not open %s\n", fname);
        std::exit(EXIT_FAILURE);
    }

    float triangle[13];
    unsigned int num_triangles = 0;
    std::fseek(stream, 80, SEEK_SET);  // Skip past header
    std::fread(&num_triangles, 4, 1, stream);
    mesh.vertices.reserve(9 * num_triangles);  // Do preallocation
    while (std::fread(triangle, 50, 1, stream)) {
        for (int j = 0; j < 9; j += 3) {
            // Skip triangle normal
            mesh.vertices.push_back(triangle[3 + j + 0]);
            mesh.vertices.push_back(triangle[3 + j + 1]);
            mesh.vertices.push_back(triangle[3 + j + 2]);
        }
    }
    std::fclose(stream);
    assert(mesh.vertices.size() == 9 * num_triangles);
}

void write_vtk(const char *fname, const unsigned char *image, int w, int h, int d,
               float spacing = 1.0f)
{
    FILE *stream = std::fopen(fname, "wb+");
    if (!stream) {
        std::fprintf(stderr, "Could not open %s\n", fname);
        std::exit(EXIT_FAILURE);
    }

    std::fprintf(stream, "# vtk DataFile Version 3.0\n");
    std::fprintf(stream, "VTK File\nBINARY\nDATASET STRUCTURED_POINTS\n");
    std::fprintf(stream, "DIMENSIONS %d %d %d\n", w, h, d);
    std::fprintf(stream, "SPACING %f %f %f\n", spacing, spacing, spacing);
    std::fprintf(stream, "ORIGIN 0 0 0\n");
    std::fprintf(stream, "POINT_DATA %ld\n", (size_t)w * h * d);
    std::fprintf(stream, "SCALARS scalars unsigned_char 1\n");
    std::fprintf(stream, "LOOKUP_TABLE default\n");
    for (size_t i = 0; i < (size_t)w * h * d; i += 4096) {
        std::fwrite(&image[i], 4096, 1, stream);
    }
    std::fclose(stream);
}
