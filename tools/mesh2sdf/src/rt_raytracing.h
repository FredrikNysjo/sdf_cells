#pragma once

#include <vector>
#include <string>

namespace rt {

void setup_scene(const std::string &mesh_filename);
void generate_sdf(uint8_t *image, int w, int h, int d);

}  // namespace rt
