#include "rt_raytracing.h"
#include "cg_utils2.h"  // Used for OBJ-mesh loading

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/component_wise.hpp>

#undef NDEBUG
#include <cassert>

namespace rt {

struct Ray {
    glm::vec3 origin;
    glm::vec3 direction;
};

struct HitRecord {
    float t;
    glm::vec3 normal;
};

struct Box {
    glm::vec3 center;
    glm::vec3 radius;
};

struct Triangle {
    glm::vec3 v0, v1, v2;
};

struct SimpleBVH {
    int32_t gridSize = 4;
    std::vector<Box> boxes;
    std::vector<int32_t> counts;
    std::vector<int32_t> offsets;
    std::vector<int32_t> data;
};

struct Scene {
    std::vector<Triangle> mesh;
    Box meshBBox;
    SimpleBVH meshBVH;
} g_scene;

// Ray-box test adapted from branchless method at
// https://tavianator.com/fast-branchless-raybounding-box-intersections/
bool box_hit(const Box &box, const Ray &ray, float t_min, float t_max, HitRecord &rec)
{
    glm::vec3 oc = ray.origin - box.center;
    glm::vec3 t0 = (-box.radius - oc) / ray.direction;
    glm::vec3 t1 = (box.radius - oc) / ray.direction;
    float temp1 = glm::compMax(glm::min(t0, t1));
    float temp2 = glm::compMin(glm::max(t1, t0));
    float temp = (temp1 < t_max && temp1 > t_min) ? temp1 : temp2;
    if (temp1 <= temp2 && temp1 < t_max && temp > t_min) {
        glm::vec3 p = ray.origin + ray.direction * temp;
        glm::vec3 npc = (p - box.center) / box.radius;
        glm::vec3 n = glm::sign(npc) * glm::step(glm::compMax(glm::abs(npc)), glm::abs(npc));
        rec.t = temp;
        rec.normal = n;
        return true;
    }
    return false;
}

// Ray-triangle test adapted from "Real-Time Collision Detection" book (pages 191--192)
bool triangle_hit(const Triangle &triangle, const Ray &ray, float t_min, float t_max,
                  HitRecord &rec)
{
    const glm::vec3 &v0 = triangle.v0;
    const glm::vec3 &v1 = triangle.v1;
    const glm::vec3 &v2 = triangle.v2;

    glm::vec3 n = glm::cross(v1 - v0, v2 - v0);
    glm::vec3 e = glm::cross(-ray.direction, ray.origin - v0);
    float d = glm::dot(-ray.direction, n);
    float temp = glm::dot(ray.origin - v0, n);
    float s = temp >= 0.0f ? 1.0f : -1.0f;
    float v = glm::dot(s >= 0.0f ? v2 - v0 : v1 - v0, e);
    float w = -glm::dot(s >= 0.0f ? v1 - v0 : v2 - v0, e);
    if ((v >= 0.0f && v <= d * s && w >= 0.0f && v + w <= d * s) ||
        (-v >= 0.0f && -v <= -d * s && -w >= 0.0f && -v + -w <= -d * s)) {
        temp /= d;
        if (temp < t_max && temp > t_min) {
            rec.t = temp;
            rec.normal = n;
            return true;
        }
    }
    return false;
}

void bvh_create_from_mesh(SimpleBVH &bvh, const std::vector<Triangle> &mesh, const Box &meshBBox)
{
    const int32_t gridSize = bvh.gridSize;
    const int32_t nBoxes = gridSize * gridSize * gridSize;

    const glm::mat4 worldFromGrid = glm::translate(glm::mat4(1.0f), meshBBox.center) *
                                    glm::scale(glm::mat4(1.0f), meshBBox.radius) *
                                    glm::scale(glm::mat4(1.0f), 2.0f / glm::vec3(gridSize)) *
                                    glm::translate(glm::mat4(1.0f), -0.5f * glm::vec3(gridSize));
    const glm::mat4 gridFromWorld = glm::inverse(worldFromGrid);

    // Create 1st level bounding box and 2nd level bounding box grid
    bvh.boxes.clear();
    bvh.boxes.resize(nBoxes + 1);
    bvh.boxes[0] = meshBBox;  // 1st level is the mesh bounding box
    for (int32_t z = 0; z < gridSize; ++z) {
        for (int32_t y = 0; y < gridSize; ++y) {
            for (int32_t x = 0; x < gridSize; ++x) {
                // Compute center and radius for 2nd level bounding box
                glm::vec3 gridCenter = glm::vec3(x, y, z) + 0.5f;
                glm::vec3 gridRadius = glm::vec3(0.5f);
                glm::vec3 worldCenter = glm::vec3(worldFromGrid * glm::vec4(gridCenter, 1.0f));
                glm::vec3 worldRadius = glm::vec3(worldFromGrid * glm::vec4(gridRadius, 0.0f));

                int32_t boxIndex = z * gridSize * gridSize + y * gridSize + x + 1;
                bvh.boxes[boxIndex].center = worldCenter;
                bvh.boxes[boxIndex].radius = worldRadius;
            }
        }
    }

    // Count the number of triangles in each bounding box. If all vertices are
    // within the same 2nd level bounding box, we will bin the triangle into
    // that box; otherwise, it will go into the large 1st level bounding box.
    bvh.counts.clear();
    bvh.counts.resize(nBoxes + 1);
    for (int32_t i = 0; i < mesh.size(); ++i) {
        glm::ivec3 v0 = glm::ivec3(gridFromWorld * glm::vec4(mesh[i].v0, 1.0f));
        glm::ivec3 v1 = glm::ivec3(gridFromWorld * glm::vec4(mesh[i].v1, 1.0f));
        glm::ivec3 v2 = glm::ivec3(gridFromWorld * glm::vec4(mesh[i].v2, 1.0f));

        int32_t boxIndex = 0;
        if (glm::all(glm::equal(v0, v1)) && glm::all(glm::equal(v0, v2)))
            boxIndex = v0.z * gridSize * gridSize + v0.y * gridSize + v0.x + 1;

        bvh.counts[boxIndex] += 1;
    }

    // Compute initial offsets for each bounding box
    bvh.offsets.clear();
    bvh.offsets.resize(nBoxes + 1);
    for (int32_t i = 0; i < nBoxes + 1; ++i) {
        bvh.offsets[i] = bvh.offsets[glm::max(0, i - 1)] + bvh.counts[i];
    }
    assert(mesh.size() == bvh.offsets[nBoxes]);

    // Write triangle indexes and compute final offsets
    bvh.data.clear();
    bvh.data.resize(mesh.size());
    for (int32_t i = 0; i < mesh.size(); ++i) {
        glm::ivec3 v0 = glm::ivec3(gridFromWorld * glm::vec4(mesh[i].v0, 1.0f));
        glm::ivec3 v1 = glm::ivec3(gridFromWorld * glm::vec4(mesh[i].v1, 1.0f));
        glm::ivec3 v2 = glm::ivec3(gridFromWorld * glm::vec4(mesh[i].v2, 1.0f));

        int32_t boxIndex = 0;
        if (glm::all(glm::equal(v0, v1)) && glm::all(glm::equal(v0, v2)))
            boxIndex = v0.z * gridSize * gridSize + v0.y * gridSize + v0.x + 1;

        // Note: in a parallel implementation, decrementing the offset should be
        // implemented as an atomic operation
        bvh.offsets[boxIndex] -= 1;
        int offset = bvh.offsets[boxIndex];
        bvh.data[offset] = i;  // Write triangle index
    }
}

void setup_scene(const std::string &filename)
{
    cg::OBJMesh mesh;
    cg::objMeshLoad(mesh, filename);

    g_scene.mesh.clear();
    glm::vec3 aabb[2] = {glm::vec3(9999.0f), glm::vec3(-9999.0f)};
    for (int32_t i = 0; i < mesh.indices.size(); i += 3) {
        int32_t i0 = mesh.indices[i + 0];
        int32_t i1 = mesh.indices[i + 1];
        int32_t i2 = mesh.indices[i + 2];
        glm::vec3 v0 = mesh.vertices[i0] + glm::vec3(0.0f, 0.135f, 0.0f);
        glm::vec3 v1 = mesh.vertices[i1] + glm::vec3(0.0f, 0.135f, 0.0f);
        glm::vec3 v2 = mesh.vertices[i2] + glm::vec3(0.0f, 0.135f, 0.0f);
        g_scene.mesh.push_back({v0, v1, v2});

        aabb[0] = glm::min(aabb[0], v0);
        aabb[1] = glm::max(aabb[1], v0);
        aabb[0] = glm::min(aabb[0], v1);
        aabb[1] = glm::max(aabb[1], v1);
        aabb[0] = glm::min(aabb[0], v2);
        aabb[1] = glm::max(aabb[1], v2);
    }
    g_scene.meshBBox.center = (aabb[1] + aabb[0]) * 0.5f;
    g_scene.meshBBox.radius = (aabb[1] - aabb[0]) * 0.5f;

    bvh_create_from_mesh(g_scene.meshBVH, g_scene.mesh, g_scene.meshBBox);
}

void generate_sdf(uint8_t *image, int32_t w, int32_t h, int32_t d)
{
    const glm::vec3 rayDirs[] = {
        glm::vec3(1.0f, 0.0f, 0.0f),
        glm::vec3(0.0f, 1.0f, 0.0f),
        glm::vec3(0.0f, 0.0f, 1.0f),
    };
    const int32_t nDirs = sizeof(rayDirs) / sizeof(rayDirs[0]);

    const Box &meshBBox = g_scene.meshBBox;      // TODO
    const SimpleBVH &meshBVH = g_scene.meshBVH;  // TODO

    const glm::mat4 worldFromVoxel =
        glm::translate(glm::mat4(1.0f), meshBBox.center) *
        glm::scale(glm::mat4(1.0f), glm::vec3(1.1f)) *
        glm::scale(glm::mat4(1.0f), glm::vec3(glm::compMax(meshBBox.radius))) *
        glm::scale(glm::mat4(1.0f), 2.0f / glm::vec3(w, h, d)) *
        glm::translate(glm::mat4(1.0f), -0.5f * glm::vec3(w, h, d));
    const glm::mat4 voxelFromWorld = glm::inverse(worldFromVoxel);

    for (int32_t z = 0; z < d; z++) {
#pragma omp parallel for schedule(dynamic)
        for (int32_t y = 0; y < h; y++) {
            for (int32_t x = 0; x < w; x++) {
                glm::vec3 voxelPos = glm::vec3(x, y, z);
                glm::vec4 worldPos = worldFromVoxel * glm::vec4(voxelPos, 1.0f);

                // Trace directions to obtain sign and minimum distance
                int32_t nInside = 0;
                float worldDist = 9999.0f;
                for (int dir = 0; dir < nDirs; ++dir) {
                    // Construct ray in world space
                    Ray ray = {glm::vec3(worldPos), rayDirs[dir]};

                    // Trace mesh for intersection count and closest distance
                    int32_t nHits[2] = {0};
                    HitRecord rec;
                    for (int32_t j = 0; j < meshBVH.boxes.size(); ++j) {
                        if (!box_hit(meshBVH.boxes[j], ray, -9999.0f, 9999.0f, rec)) continue;

                        int32_t offset = meshBVH.offsets[j];
                        for (int32_t i = 0; i < meshBVH.counts[j]; ++i) {
                            int32_t index = meshBVH.data[offset + i];
                            if (triangle_hit(g_scene.mesh[index], ray, -9999.0f, 9999.0f, rec)) {
                                if (rec.t < 0.0f) nHits[0] += 1;
                                if (rec.t >= 0.0f) nHits[1] += 1;
                                worldDist = glm::min(worldDist, glm::abs(rec.t));
                            }
                        }
                    }
                    nInside += (nHits[0] & 1) + (nHits[1] & 1);
                }

                // Write signed distance to volume
                float voxelDist = worldDist * glm::length(voxelFromWorld[0]);
                float signedDist = nInside <= nDirs ? -voxelDist : voxelDist;
                float outputDist = glm::clamp(signedDist * 7.5f + 127.5f, 0.0f, 255.0f);
                image[(z * h * w) + (y * w) + x] = uint8_t(outputDist);
            }
        }
    }
}

}  // namespace rt
