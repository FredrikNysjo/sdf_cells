/// This tool generates a signed distance field (SDF) volume from an OBJ-mesh,
/// and outputs it as a VTK structured grid file.
///
/// Author: Fredrik Nysjö (2021)
///

#include "rt_raytracing.h"
#include "cg_utils2.h"
#include "utils.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <cstdlib>
#include <iostream>
#include <string>

int main(int argc, char *argv[])
{
    if (argc < 2) {
        std::fprintf(stdout, "Usage: mesh2sdf input.obj [output.vtk]\n");
        std::exit(EXIT_SUCCESS);
    }
    const std::string inputFilename = argv[1];
    const std::string outputFilename = argc > 2 ? argv[2] : "output.vtk";

    const double tic0 = time();
    std::fprintf(stdout, "Loading OBJ mesh...\n");
    rt::setup_scene(inputFilename);
    std::fprintf(stdout, "Done (time elapsed: %lf s)\n", time() - tic0);

    const double tic1 = time();
    std::fprintf(stdout, "Generating SDF volume...\n");
    int32_t w, h, d;
    w = h = d = 128;
    std::vector<uint8_t> volume(d * h * w);
    rt::generate_sdf(&volume[0], w, h, d);
    std::fprintf(stdout, "Done (time elapsed: %lf s)\n", time() - tic1);

    const double tic2 = time();
    std::fprintf(stdout, "Writing volume to VTK file...\n");
    write_vtk("output.vtk", &volume[0], w, h, d);
    std::fprintf(stdout, "Done (time elapsed: %lf s)\n", time() - tic2);

    std::exit(EXIT_SUCCESS);
}
