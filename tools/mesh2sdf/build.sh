#!/bin/bash

if [ -z "$MESH2SDF_GENERATOR" ]; then
    MESH2SDF_GENERATOR="Unix Makefiles"
    if [[ "$OSTYPE" == "msys" ]]; then
        MESH2SDF_GENERATOR="Visual Studio 16 2019"
    fi
fi

# Generate build files
if [ ! -d build ]; then
    mkdir build
fi
cd build && \
cmake -G "$MESH2SDF_GENERATOR" ../ -DCMAKE_INSTALL_PREFIX=../ && \

# Build and install the program
cmake --build . --config RelWithDebInfo --target install && \

# Run the program
cd .. && \
`find ./bin -executable -type f` ./models/bunny_lowpoly.obj
